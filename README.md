# ioasys API

API that implements a simple ERP system using node.js, mysql, express and typeorm.

```sh
npm i
npm run dev
```

Postman documentation: https://documenter.getpostman.com/view/12499529/Uyr8kczR
