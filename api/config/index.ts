import 'reflect-metadata';
import { DataSource } from 'typeorm';
import { TypeBuyer } from '../src/buyer/persistence.typeorm/entity/TypeBuyer';
import { TypeBuyOrder } from '../src/buyOrder/persistence.typeorm/entity/TypeBuyOrder';
import { TypeManager } from '../src/manager/persistence.typeorm/entity/TypeManager';
import { TypeProduct } from '../src/product/persistence.typeorm/entity/TypeProduct';
import { TypeSeller } from '../src/seller/persistence.typeorm/entity/TypeSeller';
import { TypeSellOrder } from '../src/sellOrder/persistence.typeorm/entity/TypeSellOrder';

export const AppDataSource = new DataSource({
	type: 'mysql',
	host: 'localhost',
	port: 3306,
	username: 'admin',
	password: 'admin',
	database: 'ioasys_db',
	synchronize: true,
	logging: false,
	entities: [
		TypeManager,
		TypeProduct,
		TypeSeller,
		TypeBuyer,
		TypeSellOrder,
		TypeBuyOrder,
	],
	migrations: [],
	subscribers: [],
});
