import { CreateRepository } from '../application/db/contract/CreateRepository';
import { DeleteRepository } from '../application/db/contract/DeleteRepository';
import { FindRepository } from '../application/db/contract/FindRepository';
import { UpdateRepository } from '../application/db/contract/UpdateRepository';
import { Manager } from '../entity/Manager';

export class MockManagerRepository
	implements
		CreateRepository,
		FindRepository,
		DeleteRepository,
		UpdateRepository
{
	private managers: Manager[] = [
		{
			id: 'fasdfasdfasdf',
			email: 'manager@gmail.com',
			password: '$2b$10$uTWeTPtFUVMFV0idW6rBZOQxzhhTH63GBhxNQVWe3AHrEvejNv.86',
			mba: true,
			age: 123,
			name: 'manager',
			phone: '1234567891',
			salary: 23123,
		},
	];

	//Create
	async create(manager: Manager): Promise<void> {
		this.managers.push(manager);
	}

	//Find
	async findById(id: string): Promise<Manager> {
		const manager = await this.managers.find((s) => s.id === id);

		return manager;
	}
	async getAllManagers(): Promise<Manager[]> {
		return await this.managers;
	}
	async findByEmail(email: string): Promise<Manager> {
		const manager = this.managers.find((s) => s.email === email);
		return manager;
	}

	//Delete
	async delete(manager: Manager): Promise<void> {
		const i = this.managers.findIndex((sel) => sel.email === manager.email);
		if (i !== -1) {
			this.managers[i] = null;
		}
	}

	//Update
	async update(manager: Manager): Promise<void> {
		const i = this.managers.findIndex((sel) => sel.email === manager.email);
		if (i !== -1) {
			this.managers[i] = manager;
		}
	}
}
