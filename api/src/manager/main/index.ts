import { DbAddManagerUsecase } from '../application/db/usecase/DbAddManagerUsecase';
import { DbFindAllManagersUsecase } from '../application/db/usecase/DbFindAllManagersUsecase';
import { DbLogin } from '../application/db/usecase/DbLogin';
import { CryptoAdapter } from '../../common/infrastructure/CryptoAdapter';
import { JwtAdapter } from '../../common/infrastructure/JwtAdapter';
import { uuidV4Adapter } from '../../common/infrastructure/uuidV4Adapter';
import { MockManagerRepository } from '../persistence.mock/MockManagerRepository';
import { AuthenticantionController } from '../presentation/controller/AuthenticationController';
import { CreateManagerController } from '../presentation/controller/CreateManagerController';
import { FindAllManagersController } from '../presentation/controller/FindAllManagersController';
import { LoginResponse } from '../presentation/response/LoginResponse';
import { ManagerResponse } from '../presentation/response/ManagerResponse';
import { DbAuthenticationUsecase } from '../application/db/usecase/DbAuthenticationUsecase';
import { UserRequest } from '../../common/presentation/request/UserResquest';
import { AuthMiddleware } from '../presentation/middleware/AuthMiddleware';
import { TypeCreateRepository } from '../persistence.typeorm/adapter/TypeCreateRepository';
import { TypeFindRepository } from '../persistence.typeorm/adapter/TypeFindRepository';
import { TypeUpdateRepository } from '../persistence.typeorm/adapter/TypeUpdateRepository';
import { TypeManager } from '../persistence.typeorm/entity/TypeManager';

const crypto = new CryptoAdapter(10);
const uuid = new uuidV4Adapter();
const jwt = new JwtAdapter();

// const mockManager = new MockManagerRepository();

const typeManager = new TypeManager();

export const findManagerRepository = new TypeFindRepository();
export const createManagerRepository = new TypeCreateRepository(typeManager);
export const updateManagerRepository = new TypeUpdateRepository(
	findManagerRepository
);

const managerResponse = new ManagerResponse();
const addManagerUsecase = new DbAddManagerUsecase(
	findManagerRepository,
	createManagerRepository,
	uuid,
	crypto,
	managerResponse
);
const authCkeck = new DbAuthenticationUsecase(findManagerRepository, jwt);
const authMiddleware = new AuthMiddleware(authCkeck);
const userRequest = new UserRequest();
export const createManagerController = new CreateManagerController(
	addManagerUsecase,
	authMiddleware,
	userRequest
);

const findAllManagersUsecase = new DbFindAllManagersUsecase(
	findManagerRepository,
	managerResponse
);
export const findAllManagersController = new FindAllManagersController(
	findAllManagersUsecase
);

const loginResponse = new LoginResponse();
const login = new DbLogin(crypto, findManagerRepository, jwt, loginResponse);
export const authController = new AuthenticantionController(login, userRequest);
