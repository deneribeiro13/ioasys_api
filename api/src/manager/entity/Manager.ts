import { User } from '../../common/entity/User';

export class Manager extends User {
	mba: boolean;
	constructor(props, id: string) {
		super({ ...props, id });
		this.mba = props.mba;
	}
}
