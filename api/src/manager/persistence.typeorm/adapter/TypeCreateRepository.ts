import { CreateRepository } from '../../application/db/contract/CreateRepository';
import { Manager } from '../../entity/Manager';
import { TypeManager } from '../entity/TypeManager';
import { AppDataSource } from '../../../../config';

export class TypeCreateRepository implements CreateRepository {
	constructor(private typeManager: TypeManager) {}

	async create(entity: Manager): Promise<void> {
		const manager = this.typeManager.of(entity);
		const managerRepository = AppDataSource.getRepository(TypeManager);
		await managerRepository.save(manager);
	}
}
