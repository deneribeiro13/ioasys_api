import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { UpdateRepository } from '../../application/db/contract/UpdateRepository';
import { Manager } from '../../entity/Manager';
import { TypeManager } from '../entity/TypeManager';

export class TypeUpdateRepository implements UpdateRepository {
	constructor(private findRepository: FindRepository) {}

	async update(user: Manager): Promise<void> {
		const managerRepository = AppDataSource.getRepository(TypeManager);
		const oldManager = await this.findRepository.findById(user.id);
		Object.assign(oldManager, user);
		managerRepository.save(oldManager);
	}
}
