import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { Manager } from '../../entity/Manager';
import { TypeManager } from '../entity/TypeManager';

export class TypeFindRepository implements FindRepository {
	constructor() {}

	async findByEmail(email: string): Promise<Manager> {
		const managerRepository = AppDataSource.getRepository(TypeManager);
		const manager = await managerRepository.findOneBy({ email });
		return manager;
	}

	async findById(id: string): Promise<Manager> {
		const managerRepository = AppDataSource.getRepository(TypeManager);
		const manager = await managerRepository.findOneBy({ id });
		return manager;
	}

	async getAllManagers(): Promise<Manager[]> {
		const managerRepository = AppDataSource.getRepository(TypeManager);
		const manager = await managerRepository.find();
		return manager;
	}
}
