import {
	Column,
	CreateDateColumn,
	DeleteDateColumn,
	Entity,
	PrimaryColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Manager } from '../../entity/Manager';

@Entity()
export class TypeManager {
	@PrimaryColumn('uuid')
	id: string;

	@Column('char', { length: 50, nullable: false })
	email: string;

	@Column('char', { length: 60, nullable: false })
	password: string;

	@Column('char', { length: 50, nullable: true })
	name: string;

	@Column('integer', { nullable: true })
	age: number;

	@Column('char', { length: 15, nullable: true })
	phone: string;

	@Column('double', { nullable: true })
	salary: number;

	@Column('bool', { nullable: true })
	mba: boolean;

	@CreateDateColumn()
	createdAt: string;

	@UpdateDateColumn()
	updatedAt: string;

	@DeleteDateColumn()
	deletedAt: string;

	constructor(manager?: Manager) {
		if (manager) {
			this.id = manager.id;
			this.password = manager.password;
			this.email = manager.email;
			this.name = manager.name;
			this.age = manager.age;
			this.phone = manager.phone;
			this.salary = manager.salary;
			this.mba = manager.mba;
		}
	}

	of(manager: Manager): TypeManager {
		return new TypeManager(manager);
	}
}
