import { CreateManagerDTO } from './CreateManagerDTO';

export class UpdateManagerDTO {
	manager: CreateManagerDTO;
	managerId: string;

	constructor(props: UpdateManagerDTO) {
		Object.assign(this, props);
	}
}
