export class CreateManagerDTO {
	email: string;
	password: string;
	name: string;
	age: number;
	phone: string;
	salary: number;
	mba: boolean;

	constructor(props: CreateManagerDTO) {
		this.email = props.email;
		this.password = props.password;
		this.name = props.name;
		this.age = props.age;
		this.phone = props.phone;
		this.salary = props.salary;
		this.mba = props.mba;
	}
}
