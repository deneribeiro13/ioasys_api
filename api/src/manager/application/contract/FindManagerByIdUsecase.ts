import { ManagerPresentation } from '../presentation/ManagerPresentation';

export interface FindManagerByIdUsecase {
	execute(id: string): Promise<ManagerPresentation>;
}
