import { ManagerPresentation } from '../presentation/ManagerPresentation';

export interface FindAllManagersUsecase {
	execute(): Promise<ManagerPresentation[]>;
}
