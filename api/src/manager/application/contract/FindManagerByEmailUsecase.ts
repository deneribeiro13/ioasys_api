import { ManagerPresentation } from '../presentation/ManagerPresentation';

export interface FindManagerByEmailUsecase {
	execute(email: string): Promise<ManagerPresentation>;
}
