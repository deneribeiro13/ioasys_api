export interface DeleteManagerUsecase {
	execute(id: string): Promise<void>;
}
