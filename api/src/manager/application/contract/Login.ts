import { LoginDTO } from '../dto/LoginDTO';
import { LoginPresentation } from '../../../common/presentation/response/LoginPresentation';

export interface Login {
	execute(dto: LoginDTO): Promise<LoginPresentation>;
}
