import { CreateManagerDTO } from '../dto/CreateManagerDTO';
import { ManagerPresentation } from '../presentation/ManagerPresentation';

export interface AddManagerUsecase {
	execute(dto: CreateManagerDTO): Promise<ManagerPresentation>;
}
