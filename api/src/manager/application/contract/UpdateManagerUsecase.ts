import { UpdateManagerDTO } from '../dto/UpdateManagerDTO';

export interface UpdateManagerInterface {
	execute(dto: UpdateManagerDTO): Promise<void>;
}
