import { Manager } from '../../entity/Manager';

export interface ManagerPresentation {
	of(manager: Manager): ManagerPresentation;
}
