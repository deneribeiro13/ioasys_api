import { Manager } from '../../../entity/Manager';

export interface FindRepository {
	findByEmail(email: string): Promise<Manager>;
	findById(id: string): Promise<Manager>;
	getAllManagers(): Promise<Manager[]>;
}
