import { Manager } from '../../../entity/Manager';

export interface UpdateRepository {
	update(user: Manager): Promise<void>;
}
