import { Manager } from '../../../entity/Manager';

export interface DeleteRepository {
	delete(manager: Manager): Promise<void>;
}
