import { Manager } from '../../../entity/Manager';

export interface CreateRepository {
	create(entity: Manager): Promise<void>;
}
