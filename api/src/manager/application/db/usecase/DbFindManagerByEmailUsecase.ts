import { FindManagerByEmailUsecase } from '../../contract/FindManagerByEmailUsecase';
import { ManagerPresentation } from '../../presentation/ManagerPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindManagerByEmailUsecase implements FindManagerByEmailUsecase {
	constructor(
		private findRepository: FindRepository,
		private managerPresentation: ManagerPresentation
	) {}

	async execute(email: string): Promise<ManagerPresentation> {
		const manager = await this.findRepository.findByEmail(email);

		return this.managerPresentation.of(manager);
	}
}
