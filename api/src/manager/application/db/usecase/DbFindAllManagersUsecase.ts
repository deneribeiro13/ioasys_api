import { Manager } from '../../../entity/Manager';
import { FindAllManagersUsecase } from '../../contract/FindAllManagersUsecase';
import { ManagerPresentation } from '../../presentation/ManagerPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindAllManagersUsecase implements FindAllManagersUsecase {
	constructor(
		private findRepository: FindRepository,
		private ManagerPresentaion: ManagerPresentation
	) {}

	async execute(): Promise<ManagerPresentation[]> {
		const Managers = await this.findRepository.getAllManagers();
		return Managers.map((Manager) => {
			return this.ManagerPresentaion.of(Manager);
		});
	}
}
