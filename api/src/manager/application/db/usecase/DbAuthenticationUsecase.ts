import { TokenAdapter } from '../../../../common/adapter/TokenAdapter';
import { AuthenticationUsecase } from '../../contract/AuthenticationUsecase';
import { FindRepository } from '../contract/FindRepository';

export class DbAuthenticationUsecase implements AuthenticationUsecase {
	constructor(
		private findRepository: FindRepository,
		private tokenAdapter: TokenAdapter
	) {}

	async execute(token: string, key: string): Promise<string> {
		const decoded = this.tokenAdapter.verify(token, key);
		if (!decoded) return null;
		const manager = await this.findRepository.findByEmail(decoded.email);
		if (!manager) return null;
		return manager.email;
	}
}
