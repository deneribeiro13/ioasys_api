import { FindManagerByIdUsecase } from '../../contract/FindManagerByIdUsecase';
import { ManagerPresentation } from '../../presentation/ManagerPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindManagerByIdUsecase implements FindManagerByIdUsecase {
	constructor(
		private findRepository: FindRepository,
		private managerPresentation: ManagerPresentation
	) {}

	async execute(id: string): Promise<ManagerPresentation> {
		const manager = await this.findRepository.findById(id);
		return this.managerPresentation.of(manager);
	}
}
