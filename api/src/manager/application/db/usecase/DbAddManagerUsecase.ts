import { HashAdapter } from '../../../../common/adapter/HashAdapter';
import { uuidAdapter } from '../../../../common/adapter/uuidAdapter';
import { Manager } from '../../../entity/Manager';
import { AddManagerUsecase } from '../../contract/AddManagerUsecase';
import { CreateManagerDTO } from '../../dto/CreateManagerDTO';
import { ManagerPresentation } from '../../presentation/ManagerPresentation';
import { CreateRepository } from '../contract/CreateRepository';
import { FindRepository } from '../contract/FindRepository';

export class DbAddManagerUsecase implements AddManagerUsecase {
	constructor(
		private findRepository: FindRepository,
		private insertRepository: CreateRepository,
		private uuidAdapter: uuidAdapter,
		private hashAdapter: HashAdapter,
		private managerPresentation: ManagerPresentation
	) {}

	async execute(dto: CreateManagerDTO): Promise<ManagerPresentation> {
		const managerExists = await this.findRepository.findByEmail(dto.email);
		if (managerExists) {
			return null;
		}

		dto.password = await this.hashAdapter.generate(dto.password);
		const manager = new Manager(dto, this.uuidAdapter.uuid());

		await this.insertRepository.create(manager);

		return this.managerPresentation.of(manager);
	}
}
