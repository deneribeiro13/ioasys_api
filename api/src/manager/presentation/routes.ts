import { Router } from 'express';
import {
	authController,
	createManagerController,
	findAllManagersController,
} from '../main';

const managerRoute = Router();

managerRoute.post('/', async (req, res) => {
	return await createManagerController.handle(req, res);
});

managerRoute.post('/admin', async (req, res) => {
	return await createManagerController.handle(req, res);
});

managerRoute.post('/login', async (req, res) => {
	return await authController.handle(req, res);
});

managerRoute.get('/', async (req, res) => {
	return await findAllManagersController.handle(req, res);
});

export { managerRoute };
