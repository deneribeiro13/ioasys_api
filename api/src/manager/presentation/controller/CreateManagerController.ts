import { Request, Response } from 'express';
import { EmailInUseError } from '../../../common/presentation/errors/EmailInUseError';
import { InvalidParamsError } from '../../../common/presentation/errors/InvalidParamsError';
import { ServerError } from '../../../common/presentation/errors/ServerError';
import { UnauthorizedError } from '../../../common/presentation/errors/UnauthorizedError';
import { UserRequest } from '../../../common/presentation/request/UserResquest';
import { AddManagerUsecase } from '../../application/contract/AddManagerUsecase';
import { CreateManagerDTO } from '../../application/dto/CreateManagerDTO';
import { AuthMiddleware } from '../middleware/AuthMiddleware';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';

export class CreateManagerController {
	constructor(
		private addManager: AddManagerUsecase,
		private authMiddleware: AuthMiddleware,
		private verifyRequest: UserRequest
	) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			if (!this.verifyRequest.verify(req.body))
				return res.status(409).json(http.Conflict(new InvalidParamsError()));

			// if (!req.headers.authorization)
			// 	return res.status(409).json(http.Unauthorized());

			// const decoded = await this.authMiddleware.handle(req, res);
			// if (!decoded) return res.status(409).json(http.Unauthorized());

			const managerResponse = await this.addManager.execute(
				new CreateManagerDTO(req.body)
			);

			if (!managerResponse)
				return res.status(400).json(http.BadRequest(new EmailInUseError()));

			return res.status(201).json(managerResponse);
		} catch (err) {
			return res
				.status(500)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
