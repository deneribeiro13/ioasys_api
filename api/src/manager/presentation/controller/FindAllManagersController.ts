import { Request, Response } from 'express';
import { FindAllManagersUsecase } from '../../application/contract/FindAllManagersUsecase';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';
import { ServerError } from '../../../common/presentation/errors/ServerError';

export class FindAllManagersController {
	constructor(private findAllManagers: FindAllManagersUsecase) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			const managerResponse = await this.findAllManagers.execute();
			return res.status(200).json(http.Ok(managerResponse));
		} catch (err) {
			return res
				.status(500)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
