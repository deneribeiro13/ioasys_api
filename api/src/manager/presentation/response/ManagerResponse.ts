import { ManagerPresentation } from '../../application/presentation/ManagerPresentation';
import { Manager } from '../../entity/Manager';

export class ManagerResponse implements ManagerPresentation {
	id: string;
	email: string;
	name: string;
	age: number;
	phone: string;
	salary: number;
	mba: boolean;

	constructor(manager?: Manager) {
		if (manager) {
			this.id = manager.id;
			this.email = manager.email;
			this.name = manager.name;
			this.age = manager.age;
			this.phone = manager.phone;
			this.salary = manager.salary;
			this.mba = manager.mba;
		}
	}

	of(manager: Manager): ManagerResponse {
		return new ManagerResponse(manager);
	}
}
