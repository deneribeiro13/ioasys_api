import { LoginPresentation } from '../../../common/presentation/response/LoginPresentation';

export class LoginResponse implements LoginPresentation {
	token: string;
	email: string;

	constructor(dto?: { token; email }) {
		if (dto) {
			this.token = dto.token;
			this.email = dto.email;
		}
	}

	of(dto: { token; email }): LoginResponse {
		return new LoginResponse(dto);
	}
}
