import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { UpdateRepository } from '../../application/db/contract/UpdateRepository';
import { Buyer } from '../../entity/Buyer';
import { TypeBuyer } from '../entity/TypeBuyer';

export class TypeUpdateRepository implements UpdateRepository {
	constructor(private findRepository: FindRepository) {}

	async update(user: Buyer): Promise<void> {
		const buyerRepository = AppDataSource.getRepository(TypeBuyer);
		const oldBuyer = await this.findRepository.findById(user.id);
		Object.assign(oldBuyer, user);
		buyerRepository.save(oldBuyer);
	}
}
