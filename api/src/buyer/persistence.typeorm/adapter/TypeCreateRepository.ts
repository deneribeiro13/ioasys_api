import { CreateRepository } from '../../application/db/contract/CreateRepository';
import { Buyer } from '../../entity/Buyer';
import { AppDataSource } from '../../../../config';
import { TypeBuyer } from '../entity/TypeBuyer';

export class TypeCreateRepository implements CreateRepository {
	constructor(private typeBuyer: TypeBuyer) {}

	async create(entity: Buyer): Promise<void> {
		const buyer = this.typeBuyer.of(entity);
		const buyerRepository = AppDataSource.getRepository(TypeBuyer);
		await buyerRepository.save(buyer);
	}
}
