import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { Buyer } from '../../entity/Buyer';
import { TypeBuyer } from '../entity/TypeBuyer';

export class TypeFindRepository implements FindRepository {
	constructor() {}

	async findByEmail(email: string): Promise<Buyer> {
		const buyerRepository = AppDataSource.getRepository(TypeBuyer);
		const buyer = await buyerRepository.findOneBy({ email });
		return buyer;
	}

	async findById(id: string): Promise<Buyer> {
		const buyerRepository = AppDataSource.getRepository(TypeBuyer);
		const buyer = await buyerRepository.findOneBy({ id });
		return buyer;
	}

	async getAllBuyers(): Promise<Buyer[]> {
		const buyerRepository = AppDataSource.getRepository(TypeBuyer);
		const buyer = await buyerRepository.find();
		return buyer;
	}
}
