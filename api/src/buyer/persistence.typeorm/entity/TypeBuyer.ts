import {
	Column,
	CreateDateColumn,
	DeleteDateColumn,
	Entity,
	JoinColumn,
	OneToOne,
	PrimaryColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Manager } from '../../../manager/entity/Manager';
import { TypeManager } from '../../../manager/persistence.typeorm/entity/TypeManager';
import { Buyer } from '../../entity/Buyer';

@Entity()
export class TypeBuyer {
	@PrimaryColumn('uuid')
	id: string;

	@Column('char', { length: 50, nullable: false })
	email: string;

	@Column('char', { length: 60, nullable: false })
	password: string;

	@Column('char', { length: 50, nullable: true })
	name: string;

	@Column('int', { nullable: true })
	age: number;

	@Column('char', { length: 15, nullable: true })
	phone: string;

	@Column('double', { nullable: true })
	salary: number;

	@Column('integer', { nullable: true })
	experince_years: number;

	@OneToOne(() => TypeManager)
	@JoinColumn()
	manager: Manager;

	@CreateDateColumn()
	createdAt: string;

	@UpdateDateColumn()
	updatedAt: string;

	@DeleteDateColumn()
	deletedAt: string;

	constructor(buyer?: Buyer) {
		if (buyer) {
			this.id = buyer.id;
			this.password = buyer.password;
			this.email = buyer.email;
			this.name = buyer.name;
			this.age = buyer.age;
			this.phone = buyer.phone;
			this.salary = buyer.salary;
			this.experince_years = buyer.experince_years;
			this.manager = buyer.manager;
		}
	}

	of(buyer: Buyer): TypeBuyer {
		return new TypeBuyer(buyer);
	}
}
