import { CreateRepository } from '../application/db/contract/CreateRepository';
import { DeleteRepository } from '../application/db/contract/DeleteRepository';
import { FindRepository } from '../application/db/contract/FindRepository';
import { UpdateRepository } from '../application/db/contract/UpdateRepository';
import { Buyer } from '../entity/Buyer';

export class MockBuyerRepository
	implements
		CreateRepository,
		FindRepository,
		DeleteRepository,
		UpdateRepository
{
	private buyers: Buyer[] = [];

	//Create
	async create(seller: Buyer): Promise<void> {
		this.buyers.push(seller);
	}

	//Find
	async findById(id: string): Promise<Buyer> {
		const buyer = await this.buyers.find((b) => b.id === id);

		return buyer;
	}
	async getAllBuyers(): Promise<Buyer[]> {
		return await this.buyers;
	}
	async findByEmail(email: string): Promise<Buyer> {
		const buyer = await this.buyers.find((b) => b.email === email);
		return buyer;
	}

	//Delete
	async delete(seller: Buyer): Promise<void> {
		const i = this.buyers.findIndex((sel) => sel.email === seller.email);
		if (i !== -1) {
			this.buyers[i] = null;
		}
	}

	//Update
	async update(seller: Buyer): Promise<void> {
		const i = this.buyers.findIndex((sel) => sel.email === seller.email);
		if (i !== -1) {
			this.buyers[i] = seller;
		}
	}
}
