import { CryptoAdapter } from '../../common/infrastructure/CryptoAdapter';
import { JwtAdapter } from '../../common/infrastructure/JwtAdapter';
import { uuidV4Adapter } from '../../common/infrastructure/uuidV4Adapter';
import { UserRequest } from '../../common/presentation/request/UserResquest';
import { findManagerRepository } from '../../manager/main';
import { DbAddBuyerUsecase } from '../application/db/usecase/DbAddBuyerUsecase';
import { DbAuthenticationUsecase } from '../application/db/usecase/DbAuthenticationUsecase';
import { DbFindAllBuyersUsecase } from '../application/db/usecase/DbFindAllBuyersUsecase';
import { DbLogin } from '../application/db/usecase/DbLogin';
import { TypeCreateRepository } from '../persistence.typeorm/adapter/TypeCreateRepository';
import { TypeFindRepository } from '../persistence.typeorm/adapter/TypeFindRepository';
import { TypeUpdateRepository } from '../persistence.typeorm/adapter/TypeUpdateRepository';
import { TypeBuyer } from '../persistence.typeorm/entity/TypeBuyer';
import { AuthenticantionController } from '../presentation/controller/AuthenticationController';
import { CreateBuyerController } from '../presentation/controller/CreateBuyerController';
import { FindAllBuyersController } from '../presentation/controller/FindAllBuyersController';
import { AuthMiddleware } from '../presentation/middleware/AuthMiddleware';
import { BuyerResponse } from '../presentation/response/BuyerResponse';
import { LoginResponse } from '../presentation/response/LoginResponse';

const crypto = new CryptoAdapter(10);
const uuid = new uuidV4Adapter();
const jwt = new JwtAdapter();

// const mockBuyer = new MockBuyerRepository();

const typeBuyer = new TypeBuyer();
export const findBuyerRepository = new TypeFindRepository();
export const createBuyerRepository = new TypeCreateRepository(typeBuyer);
export const updateBuyerRepository = new TypeUpdateRepository(
	findBuyerRepository
);

const sellerResponse = new BuyerResponse();
const addBuyerUsecase = new DbAddBuyerUsecase(
	findBuyerRepository,
	createBuyerRepository,
	uuid,
	crypto,
	sellerResponse
);

const authCheck = new DbAuthenticationUsecase(findManagerRepository, jwt);
const authMiddlerware = new AuthMiddleware(authCheck);
const verifyUser = new UserRequest();
export const createBuyerController = new CreateBuyerController(
	addBuyerUsecase,
	authMiddlerware,
	verifyUser
);

const findAllBuyersUsecase = new DbFindAllBuyersUsecase(
	findBuyerRepository,
	sellerResponse
);
export const findAllBuyersController = new FindAllBuyersController(
	findAllBuyersUsecase
);

const loginResponse = new LoginResponse();
const login = new DbLogin(crypto, findBuyerRepository, jwt, loginResponse);
export const authController = new AuthenticantionController(login, verifyUser);
