import { Request, Response } from 'express';
import { AccessDeniedError } from '../../../common/presentation/errors/AccessDeniedError';
import { InvalidParamsError } from '../../../common/presentation/errors/InvalidParamsError';
import { UserRequest } from '../../../common/presentation/request/UserResquest';
import { Login } from '../../application/contract/Login';
import { LoginDTO } from '../../application/dto/LoginDTO';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';
import { ServerError } from '../../../common/presentation/errors/ServerError';

export class AuthenticantionController {
	constructor(private login: Login, private verifyUser: UserRequest) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			if (!this.verifyUser.verify(req.body))
				return res.status(409).json(http.Conflict(new InvalidParamsError()));

			const login = await this.login.execute(new LoginDTO(req.body));
			if (!login) return res.status(401).json(http.Unauthorized());

			return res.status(200).json(http.Ok({ ...login }));
		} catch (err) {
			return res
				.status(500)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
