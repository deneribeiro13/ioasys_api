import { Request, Response } from 'express';
import { EmailInUseError } from '../../../common/presentation/errors/EmailInUseError';
import { InvalidParamsError } from '../../../common/presentation/errors/InvalidParamsError';
import { UserRequest } from '../../../common/presentation/request/UserResquest';
import { AddBuyerUsecase } from '../../application/contract/AddBuyerUsecase';
import { CreateBuyerDTO } from '../../application/dto/CreateBuyerDTO';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';
import { AuthMiddleware } from '../middleware/AuthMiddleware';
import { ServerError } from '../../../common/presentation/errors/ServerError';

export class CreateBuyerController {
	constructor(
		private addBuyer: AddBuyerUsecase,
		private authMiddlerware: AuthMiddleware,
		private verifyUser: UserRequest
	) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			if (!this.verifyUser.verify(req.body))
				return res.status(409).json(http.Conflict(new InvalidParamsError()));

			if (!req.headers.authorization)
				return res.status(401).json(http.Unauthorized());

			const email = await this.authMiddlerware.handle(req, res);

			req.body.manager = { email };

			if (!email) return res.status(401).json(http.Unauthorized());

			const buyerResponse = await this.addBuyer.execute(
				new CreateBuyerDTO(req.body)
			);
			if (!buyerResponse)
				return res.status(400).json(http.BadRequest(new EmailInUseError()));

			return res.status(200).json(http.Ok(buyerResponse));
		} catch (err) {
			return res
				.status(500)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
