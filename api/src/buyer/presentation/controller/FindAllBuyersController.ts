import { Request, Response } from 'express';
import { FindAllBuyersUsecase } from '../../application/contract/FindAllBuyersUsecase';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';
import { ServerError } from '../../../common/presentation/errors/ServerError';

export class FindAllBuyersController {
	constructor(private findAllBuyers: FindAllBuyersUsecase) {}

	async handle(req: Request, res: Response) {
		try {
			const buyerResponse = await this.findAllBuyers.execute();
			return res.status(200).json(http.Ok(buyerResponse));
		} catch (err) {
			return res
				.status(500)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
