import { Router } from 'express';
import {
	authController,
	createBuyerController,
	findAllBuyersController,
} from '../main';

const buyerRoute = Router();

buyerRoute.post('/', (req, res) => {
	return createBuyerController.handle(req, res);
});

buyerRoute.post('/login', (req, res) => {
	return authController.handle(req, res);
});

buyerRoute.get('/', (req, res) => {
	return findAllBuyersController.handle(req, res);
});

export { buyerRoute };
