import { Request, Response } from 'express';
import sanitizedConfig from '../../../../config/config';
import { AuthenticationUsecase } from '../../application/contract/AuthenticationUsecase';

export class AuthMiddleware {
	constructor(private authCkeck: AuthenticationUsecase) {}

	async handle(req: Request, res: Response): Promise<String> {
		const payload = req.headers.authorization.split(' ')[1];
		const email = await this.authCkeck.execute(payload, sanitizedConfig.SECRET);
		return email;
	}
}
