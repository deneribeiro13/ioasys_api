import { Manager } from '../../../manager/entity/Manager';
import { BuyerPresentation } from '../../application/presentation/BuyerPresentation';
import { Buyer } from '../../entity/Buyer';

export class BuyerResponse implements BuyerPresentation {
	id: string;
	email: string;
	name: string;
	age: number;
	phone: number;
	salary: number;
	experince_years: number;
	manager: Manager;

	constructor(buyer?: Buyer) {
		if (buyer) {
			this.id = buyer.id;
			this.email = buyer.email;
			this.name = buyer.name;
			this.age = buyer.age;
			this.phone = buyer.phone;
			this.salary = buyer.salary;
			this.experince_years = buyer.experince_years;
			this.manager = buyer.manager;
		}
	}

	of(buyer: Buyer): BuyerResponse {
		return new BuyerResponse(buyer);
	}
}
