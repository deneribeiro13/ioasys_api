import { CreateManagerDTO } from '../../../manager/application/dto/CreateManagerDTO';

export class CreateBuyerDTO {
	email: string;
	password: string;
	name: string;
	age: number;
	phone: number;
	salary: number;
	experince_years: number;
	manager: CreateManagerDTO;

	constructor(props: CreateBuyerDTO) {
		Object.assign(this, props);
	}
}
