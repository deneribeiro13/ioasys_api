import { CreateBuyerDTO } from './CreateBuyerDTO';

export class UpdateBuyerDTO {
	seller: CreateBuyerDTO;
	sellserId: string;

	constructor(props: UpdateBuyerDTO) {
		Object.assign(this, props);
	}
}
