import { Buyer } from '../../entity/Buyer';

export interface BuyerPresentation {
	of(buyer: Buyer): BuyerPresentation;
}
