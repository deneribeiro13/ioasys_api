import { Buyer } from '../../../entity/Buyer';
import { FindBuyerByEmailUsecase } from '../../contract/FindBuyerByEmailUsecase';
import { BuyerPresentation } from '../../presentation/BuyerPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindBuyerByEmailUsecase implements FindBuyerByEmailUsecase {
	constructor(
		private findRepository: FindRepository,
		private buyerPresentation: BuyerPresentation
	) {}

	async execute(email: string): Promise<BuyerPresentation> {
		const buyer = await this.findRepository.findByEmail(email);
		return this.buyerPresentation.of(buyer);
	}
}
