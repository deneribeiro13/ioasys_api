import { HashAdapter } from '../../../../common/adapter/HashAdapter';
import { uuidAdapter } from '../../../../common/adapter/uuidAdapter';
import { FindManagerByEmailUsecase } from '../../../../manager/application/contract/FindManagerByEmailUsecase';
import { Buyer } from '../../../entity/Buyer';
import { AddBuyerUsecase } from '../../contract/AddBuyerUsecase';
import { CreateBuyerDTO } from '../../dto/CreateBuyerDTO';
import { BuyerPresentation } from '../../presentation/BuyerPresentation';
import { CreateRepository } from '../contract/CreateRepository';
import { FindRepository } from '../contract/FindRepository';

export class DbAddBuyerUsecase implements AddBuyerUsecase {
	constructor(
		private findRepository: FindRepository,
		private insertRepository: CreateRepository,
		private uuidAdapter: uuidAdapter,
		private hashAdapter: HashAdapter,
		private buyerPresentation: BuyerPresentation
	) {}

	async execute(dto: CreateBuyerDTO): Promise<BuyerPresentation> {
		const buyerExists = await this.findRepository.findByEmail(dto.email);
		if (buyerExists) {
			return null;
		}
		dto.password = await this.hashAdapter.generate(dto.password);
		const buyer = new Buyer(dto, this.uuidAdapter.uuid());
		await this.insertRepository.create(buyer);

		return this.buyerPresentation.of(buyer);
	}
}
