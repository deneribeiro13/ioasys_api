import { Buyer } from '../../../entity/Buyer';
import { FindAllBuyersUsecase } from '../../contract/FindAllBuyersUsecase';
import { BuyerPresentation } from '../../presentation/BuyerPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindAllBuyersUsecase implements FindAllBuyersUsecase {
	constructor(
		private findRepository: FindRepository,
		private buyerPresentaion: BuyerPresentation
	) {}

	async execute(): Promise<BuyerPresentation[]> {
		const buyers = await this.findRepository.getAllBuyers();
		return buyers.map((buyer) => {
			return this.buyerPresentaion.of(buyer);
		});
	}
}
