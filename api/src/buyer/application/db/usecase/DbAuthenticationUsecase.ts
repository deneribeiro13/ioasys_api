import { TokenAdapter } from '../../../../common/adapter/TokenAdapter';
import { FindRepository } from '../../../../manager/application/db/contract/FindRepository';
import { AuthenticationUsecase } from '../../contract/AuthenticationUsecase';

export class DbAuthenticationUsecase implements AuthenticationUsecase {
	constructor(
		private findManager: FindRepository,
		private tokenAdapter: TokenAdapter
	) {}

	async execute(token: string, key: string): Promise<string> {
		const decoded = this.tokenAdapter.verify(token, key);
		if (!decoded) return null;
		const manager = await this.findManager.findById(decoded.id);
		if (!manager) return null;
		return manager.email;
	}
}
