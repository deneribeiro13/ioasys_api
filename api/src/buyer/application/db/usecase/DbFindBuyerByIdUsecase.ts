import { FindBuyerByIdUsecase } from '../../contract/FindBuyerByIdUsecase';
import { BuyerPresentation } from '../../presentation/BuyerPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindBuyerByIdUsecase implements FindBuyerByIdUsecase {
	constructor(
		private findRepository: FindRepository,
		private buyerPresentation: BuyerPresentation
	) {}
	async execute(id: string): Promise<BuyerPresentation> {
		const buyer = await this.findRepository.findById(id);
		return this.buyerPresentation.of(buyer);
	}
}
