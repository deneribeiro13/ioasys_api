import { Login } from '../../contract/Login';
import { FindRepository } from '../contract/FindRepository';
import { LoginDTO } from '../../dto/LoginDTO';
import { HashAdapter } from '../../../../common/adapter/HashAdapter';
import { LoginPresentation } from '../../../../common/presentation/response/LoginPresentation';
import { TokenAdapter } from '../../../../common/adapter/TokenAdapter';

export class DbLogin implements Login {
	constructor(
		private hashAdapter: HashAdapter,
		private findUser: FindRepository,
		private tokenAdapter: TokenAdapter,
		private loginPresentation: LoginPresentation
	) {}

	async execute(dto: LoginDTO): Promise<LoginPresentation> {
		const user = await this.findUser.findByEmail(dto.email);
		if (!user) {
			return null;
		}

		const passCompare = await this.hashAdapter.compare(
			dto.password,
			user.password
		);
		if (!passCompare) {
			return null;
		}

		const token = await this.tokenAdapter.sign({
			email: dto.email,
			id: user.id,
		});

		return this.loginPresentation.of({ token, email: dto.email });
	}
}
