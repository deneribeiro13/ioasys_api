import { Buyer } from '../../../entity/Buyer';

export interface DeleteRepository {
	delete(buyer: Buyer): Promise<void>;
}
