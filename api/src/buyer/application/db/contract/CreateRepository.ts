import { Buyer } from '../../../entity/Buyer';

export interface CreateRepository {
	create(entity: Buyer): Promise<void>;
}
