import { Buyer } from '../../../entity/Buyer';

export interface FindRepository {
	findByEmail(email: string): Promise<Buyer>;
	findById(id: string): Promise<Buyer>;
	getAllBuyers(): Promise<Buyer[]>;
}
