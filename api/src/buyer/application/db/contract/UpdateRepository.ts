import { Buyer } from '../../../entity/Buyer';

export interface UpdateRepository {
	update(user: Buyer): Promise<void>;
}
