export default interface DelteBuyerUsecase {
	execute(id: string): Promise<void>;
}
