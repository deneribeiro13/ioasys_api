import { BuyerPresentation } from '../presentation/BuyerPresentation';

export interface FindAllBuyersUsecase {
	execute(): Promise<BuyerPresentation[]>;
}
