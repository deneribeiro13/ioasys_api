import { UpdateBuyerDTO } from '../dto/UpdateBuyerDTO';

export interface UpdateBuyerUsecase {
	execute(dto: UpdateBuyerDTO): Promise<void>;
}
