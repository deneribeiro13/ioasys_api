import { BuyerPresentation } from '../presentation/BuyerPresentation';

export interface FindBuyerByIdUsecase {
	execute(id: string): Promise<BuyerPresentation>;
}
