import { CreateBuyerDTO } from '../dto/CreateBuyerDTO';
import { BuyerPresentation } from '../presentation/BuyerPresentation';

export interface AddBuyerUsecase {
	execute(dto: CreateBuyerDTO): Promise<BuyerPresentation>;
}
