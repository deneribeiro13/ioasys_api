import { BuyerPresentation } from '../presentation/BuyerPresentation';

export interface FindBuyerByEmailUsecase {
	execute(email: string): Promise<BuyerPresentation>;
}
