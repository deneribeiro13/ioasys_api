import { User } from '../../common/entity/User';
import { Manager } from '../../manager/entity/Manager';

export class Buyer extends User {
	experince_years: number;
	manager: Manager;
	constructor(props, id: string) {
		super({ ...props, id });
		this.experince_years = props.experince_years;
		this.manager = props.manager;
	}
}
