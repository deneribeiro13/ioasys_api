export class UserNotFoundError extends Error {
	constructor(user: string) {
		super(`${user} not found.`);
		this.name = 'UserNotError';
	}
}
