export class ServerError extends Error {
	constructor(err: string) {
		super('Server error.');
		this.name = 'ServerError';
	}
}
