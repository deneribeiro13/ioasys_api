export class UserRequest {
	private verifyEmail(email: string): boolean {
		const match = email
			? email
					.toLowerCase()
					.match(
						/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
					)
			: false;
		return match ? true : false;
	}

	private verifyPasword(paswd: string): boolean {
		return paswd ? paswd.length > 7 && paswd.length < 30 : false;
	}
	private verifyName(name: string): boolean {
		return name ? name.length > 0 && name.length < 50 : true;
	}
	private verifyAge(age: number): boolean {
		return age ? age > 0 && age < 200 : true;
	}
	private verifyPhone(phone: string): boolean {
		return phone ? phone.length === 13 : true;
	}
	private verifySalary(salary: number): boolean {
		return salary ? salary > 0 && salary < 10000000 : true;
	}

	verify(body): boolean {
		return (
			body &&
			this.verifyEmail(body.email) &&
			this.verifyPasword(body.password) &&
			this.verifyName(body.name) &&
			this.verifyAge(body.age) &&
			this.verifyPhone(body.phone) &&
			this.verifySalary(body.salary)
		);
	}
}
