export interface LoginPresentation {
	of(dto: { token; email }): LoginPresentation;
}
