import { uuidAdapter } from '../adapter/uuidAdapter';
import { v4 } from 'uuid';

export class uuidV4Adapter implements uuidAdapter {
	uuid(): string {
		return v4();
	}
}
