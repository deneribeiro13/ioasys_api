import { TokenAdapter } from '../adapter/TokenAdapter';
import jwt from 'jsonwebtoken';
import sanitizedConfig from '../../../config/config';

export class JwtAdapter implements TokenAdapter {
	async sign(value: object): Promise<string> {
		return jwt.sign(value, sanitizedConfig.SECRET, {
			expiresIn: '1h',
		});
	}

	verify(token: string, key: string) {
		try {
			const decoded = jwt.verify(token, key);
			return decoded;
		} catch (err) {
			return null;
		}
	}
}
