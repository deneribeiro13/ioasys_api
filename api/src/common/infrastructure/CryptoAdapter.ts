import bcrypt from 'bcrypt';
import { HashAdapter } from '../adapter/HashAdapter';

export class CryptoAdapter implements HashAdapter {
	constructor(private readonly saltRounds: number) {}

	async generate(value: string) {
		const salt = await bcrypt.genSalt(this.saltRounds || 10);
		const hash = await bcrypt.hash(value, salt);
		return hash;
	}

	async compare(value: string, hash: string) {
		return await bcrypt.compare(value, hash);
	}
}
