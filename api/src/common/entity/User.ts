export abstract class User {
	id: string;

	email: string;
	password: string;
	name: string;
	age: number;
	phone: string;
	salary: number;

	constructor(props: User) {
		Object.assign(this, props);
	}
}
