import { HttpResponse } from '../adapter/HttpResponse';
import { ServerError } from '../presentation/errors/ServerError';
import { UnauthorizedError } from '../presentation/errors/UnauthorizedError';

export class HttpResponseFactory {
	static Ok(data: any): HttpResponse {
		return { statusCode: 200, body: data };
	}

	static Created(): HttpResponse {
		return { statusCode: 201 };
	}

	static NoContent(): HttpResponse {
		return { statusCode: 204 };
	}

	static BadRequest(error: Error): HttpResponse {
		return { statusCode: 400, body: error };
	}

	static Unauthorized(): HttpResponse {
		return { statusCode: 401, body: new UnauthorizedError() };
	}

	static Forbidden(error: Error): HttpResponse {
		return { statusCode: 403, body: error };
	}

	static NotFound(error: Error): HttpResponse {
		return { statusCode: 404, body: error };
	}

	static Conflict(error: Error): HttpResponse {
		return { statusCode: 409, body: error };
	}

	static InternalServerError(error: Error): HttpResponse {
		return { statusCode: 500, body: new ServerError(error.stack) };
	}
}
