export interface TokenAdapter {
	sign(value: object): Promise<string>;
	verify(token: string, key: string);
}
