import { Product } from '../../product/entity/Product';
import { Seller } from '../../seller/entity/Seller';

export class SellOrder {
	id: string;

	number: number;
	product: Product;
	seller: Seller;

	constructor(props, id: string) {
		Object.assign(this, { ...props, id });
	}
}
