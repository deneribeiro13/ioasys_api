import { SellOrder } from '../../entity/SellOrder';

export interface SellOrderPresentation {
	of(sellORder: SellOrder): SellOrderPresentation;
}
