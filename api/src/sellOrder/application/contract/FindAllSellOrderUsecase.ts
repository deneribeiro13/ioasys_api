import { SellOrderPresentation } from '../presentation/SellOrderPresentation';

export interface FindAllSellOrderUsecase {
	execute(): Promise<SellOrderPresentation[]>;
}
