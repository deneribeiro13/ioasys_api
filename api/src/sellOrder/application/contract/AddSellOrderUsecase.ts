import { CreateSellOrderDTO } from '../dto/CreateSellOrderDTO';
import { SellOrderPresentation } from '../presentation/SellOrderPresentation';

export interface AddSellOrderUsecase {
	execute(dto: CreateSellOrderDTO): Promise<SellOrderPresentation>;
}
