import { UpdateSellOrderDTO } from '../dto/UpdateSellOrderDTO';

export interface UpdateSellOrder {
	execute(dto: UpdateSellOrderDTO): Promise<void>;
}
