import { SellOrderPresentation } from '../presentation/SellOrderPresentation';

export interface FindSellOrderByIdUsecase {
	execute(id: string): Promise<SellOrderPresentation>;
}
