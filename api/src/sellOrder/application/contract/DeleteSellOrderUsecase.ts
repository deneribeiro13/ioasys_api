export interface DeleteSellOrderUsecase {
	execute(id: string): Promise<void>;
}
