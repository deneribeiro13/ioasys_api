import { CreateSellOrderDTO } from './CreateSellOrderDTO';

export class UpdateSellOrderDTO {
	sellDto: CreateSellOrderDTO;
	sellId: string;

	constructor(props: UpdateSellOrderDTO) {
		Object.assign(this, props);
	}
}
