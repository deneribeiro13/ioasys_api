import { Product } from '../../../product/entity/Product';
import { Seller } from '../../../seller/entity/Seller';

export class CreateSellOrderDTO {
	seller: Seller;

	number: number;
	product: Product;

	constructor(props: CreateSellOrderDTO) {
		Object.assign(this, props);
	}
}
