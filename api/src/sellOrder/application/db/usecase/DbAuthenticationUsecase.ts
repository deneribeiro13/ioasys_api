import { TokenAdapter } from '../../../../common/adapter/TokenAdapter';
import { FindRepository } from '../../../../seller/application/db/contract/FindRepository';
import { AuthenticationUsecase } from '../../contract/AuthenticationUsecase';

export class DbAuthenticationUsecase implements AuthenticationUsecase {
	constructor(
		private findSeller: FindRepository,
		private tokenAdapter: TokenAdapter
	) {}

	async execute(token: string, key: string): Promise<string> {
		const decoded = this.tokenAdapter.verify(token, key);
		if (!decoded) return null;
		const seller = await this.findSeller.findById(decoded.id);
		if (!seller) return null;
		return seller.id;
	}
}
