import { uuidAdapter } from '../../../../common/adapter/uuidAdapter';
import { FindProductByBarcodeUsecase } from '../../../../product/application/contract/FindProductByBarcodeUsecase';
import { UpdateProductUsecase } from '../../../../product/application/contract/UpdateProductUsecase';
import { SellOrder } from '../../../entity/SellOrder';
import { AddSellOrderUsecase } from '../../contract/AddSellOrderUsecase';
import { CreateSellOrderDTO } from '../../dto/CreateSellOrderDTO';
import { SellOrderPresentation } from '../../presentation/SellOrderPresentation';
import { CreateRepository } from '../contract/CreateRepository';

export class DbAddSellOrderUsecase implements AddSellOrderUsecase {
	constructor(
		private insertRepository: CreateRepository,
		private findProduct: FindProductByBarcodeUsecase,
		private uuidAdapter: uuidAdapter,
		private updateProduct: UpdateProductUsecase,
		private sellOrderPresentation: SellOrderPresentation
	) {}

	async execute(dto: CreateSellOrderDTO): Promise<SellOrderPresentation> {
		const product = await this.findProduct.execute(dto.product.barcode);

		if (product.itens_in_storage - dto.number < 0) return null;

		product.itens_in_storage -= dto.number;
		await this.updateProduct.execute(product);

		dto.product = product;

		const sellOrder = new SellOrder(dto, this.uuidAdapter.uuid());
		console.log(sellOrder.id);

		await this.insertRepository.create(sellOrder);

		return this.sellOrderPresentation.of(sellOrder);
	}
}
