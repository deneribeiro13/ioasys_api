import { SellOrder } from '../../../entity/SellOrder';

export interface UpdateRepository {
	update(user: SellOrder): Promise<void>;
}
