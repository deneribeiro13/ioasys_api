import { SellOrder } from '../../../entity/SellOrder';

export interface CreateRepository {
	create(entity: SellOrder): Promise<void>;
}
