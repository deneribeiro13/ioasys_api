import { SellOrder } from '../../../entity/SellOrder';

export interface FindRepository {
	findByEmail(email: string): Promise<SellOrder>;
	findById(id: string): Promise<SellOrder>;
	getAllSellOrders(): Promise<SellOrder[]>;
}
