import { SellOrder } from '../../../entity/SellOrder';

export interface DeleteRepository {
	delete(sellOrder: SellOrder): Promise<void>;
}
