import { CreateRepository } from '../application/db/contract/CreateRepository';
import { DeleteRepository } from '../application/db/contract/DeleteRepository';
import { FindRepository } from '../application/db/contract/FindRepository';
import { UpdateRepository } from '../application/db/contract/UpdateRepository';
import { SellOrder } from '../entity/SellOrder';

export class MockSellOrderRepository
	implements
		CreateRepository,
		FindRepository,
		DeleteRepository,
		UpdateRepository
{
	private sellOrders: SellOrder[] = [];

	//Create
	async create(sellOrder: SellOrder): Promise<void> {
		this.sellOrders.push(sellOrder);
	}

	//Find
	async findById(id: string): Promise<SellOrder> {
		const sellOrder = await this.sellOrders.find((s) => s.id === id);

		return sellOrder;
	}
	async getAllSellOrders(): Promise<SellOrder[]> {
		return await this.sellOrders;
	}
	async findByEmail(email: string): Promise<SellOrder> {
		const sellOrder = await this.sellOrders.find(
			(s) => s.seller.email === email
		);

		return sellOrder;
	}

	//Delete
	async delete(sellOrder: SellOrder): Promise<void> {
		const i = this.sellOrders.findIndex((sel) => sel.id === sellOrder.id);
		if (i !== -1) {
			this.sellOrders[i] = null;
		}
	}

	//Update
	async update(sellOrder: SellOrder): Promise<void> {
		const i = this.sellOrders.findIndex((sel) => sel.id === sellOrder.id);
		if (i !== -1) {
			this.sellOrders[i] = sellOrder;
		}
	}
}
