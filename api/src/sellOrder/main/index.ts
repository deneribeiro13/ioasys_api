import { JwtAdapter } from '../../common/infrastructure/JwtAdapter';
import { uuidV4Adapter } from '../../common/infrastructure/uuidV4Adapter';
import { DbFindProductByBarcodeUsecase } from '../../product/application/db/usecase/DbFindProductByBarcode';
import { DbUpdateProductUsecase } from '../../product/application/db/usecase/DbUpdateProductUsecase';
import {
	findProductRepository,
	updateProductRepository,
} from '../../product/main';
import { ProductRequest } from '../../product/presentation/request/ProductResquest';
import { ProductResponse } from '../../product/presentation/response/ProductResponse';
import { findSellerRepository } from '../../seller/main';
import { DbAddSellOrderUsecase } from '../application/db/usecase/DbAddSellOrderUsecase';
import { DbAuthenticationUsecase } from '../application/db/usecase/DbAuthenticationUsecase';
import { TypeCreateRepository } from '../persistence.typeorm/adapter/TypeCreateRepository';
import { TypeFindRepository } from '../persistence.typeorm/adapter/TypeFindRepository';
import { TypeUpdateRepository } from '../persistence.typeorm/adapter/TypeUpdateRepository';
import { TypeSellOrder } from '../persistence.typeorm/entity/TypeSellOrder';
import { CreateSellOrderController } from '../presentation/controller/CreateSellOrderController';
import { AuthMiddleware } from '../presentation/middleware/AuthMiddleware';
import { SellOrderResponse } from '../presentation/response/SellOrderResponse';

const uuid = new uuidV4Adapter();
const jwt = new JwtAdapter();

// const mockSellOrder = new MockSellOrderRepository();

const typeSellOrder = new TypeSellOrder();
export const findSellOrderRepository = new TypeFindRepository();
export const createSellOrderRepository = new TypeCreateRepository(
	typeSellOrder
);
export const updateSellOrderRepository = new TypeUpdateRepository(
	findSellOrderRepository
);

const productResponse = new ProductResponse();
const findProductByBarcode = new DbFindProductByBarcodeUsecase(
	findProductRepository,
	productResponse
);

const updateProduct = new DbUpdateProductUsecase(
	findProductRepository,
	updateProductRepository,
	productResponse
);

const sellOrderResponse = new SellOrderResponse();
const addSellOrderUsecase = new DbAddSellOrderUsecase(
	createSellOrderRepository,
	findProductByBarcode,
	uuid,
	updateProduct,
	sellOrderResponse
);
const authCheck = new DbAuthenticationUsecase(findSellerRepository, jwt);
const authMiddleware = new AuthMiddleware(authCheck);
const verifyProduct = new ProductRequest();
export const createSellOrderController = new CreateSellOrderController(
	addSellOrderUsecase,
	authMiddleware,
	verifyProduct
);
