import {
	Column,
	CreateDateColumn,
	DeleteDateColumn,
	Entity,
	JoinColumn,
	OneToOne,
	PrimaryColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Product } from '../../../product/entity/Product';
import { TypeProduct } from '../../../product/persistence.typeorm/entity/TypeProduct';
import { Seller } from '../../../seller/entity/Seller';
import { TypeSeller } from '../../../seller/persistence.typeorm/entity/TypeSeller';
import { SellOrder } from '../../entity/SellOrder';

@Entity()
export class TypeSellOrder {
	@PrimaryColumn('uuid')
	id: string;

	@Column('integer', { nullable: false })
	number: number;

	@OneToOne(() => TypeProduct, { nullable: false })
	@JoinColumn()
	product: Product;

	@OneToOne(() => TypeSeller, { nullable: false })
	@JoinColumn()
	seller: Seller;

	@CreateDateColumn()
	createdAt: string;

	@UpdateDateColumn()
	updatedAt: string;

	@DeleteDateColumn()
	deletedAt: string;

	constructor(sellOrder?: SellOrder) {
		if (sellOrder) {
			this.id = sellOrder.id;
			this.number = sellOrder.number;
			this.product = sellOrder.product;
			this.seller = sellOrder.seller;
		}
	}

	of(sellOrder: SellOrder): TypeSellOrder {
		return new TypeSellOrder(sellOrder);
	}
}
