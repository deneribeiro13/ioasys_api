import { CreateRepository } from '../../application/db/contract/CreateRepository';
import { SellOrder } from '../../entity/SellOrder';
import { TypeSellOrder } from '../entity/TypeSellOrder';
import { AppDataSource } from '../../../../config';

export class TypeCreateRepository implements CreateRepository {
	constructor(private typeSellOrder: TypeSellOrder) {}

	async create(entity: SellOrder): Promise<void> {
		const sellOrder = this.typeSellOrder.of(entity);
		const sellOrderRepository = AppDataSource.getRepository(TypeSellOrder);
		await sellOrderRepository.save(sellOrder);
	}
}
