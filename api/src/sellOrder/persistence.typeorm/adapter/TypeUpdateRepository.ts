import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { UpdateRepository } from '../../application/db/contract/UpdateRepository';
import { SellOrder } from '../../entity/SellOrder';
import { TypeSellOrder } from '../entity/TypeSellOrder';

export class TypeUpdateRepository implements UpdateRepository {
	constructor(private findRepository: FindRepository) {}

	async update(user: SellOrder): Promise<void> {
		const sellOrderRepository = AppDataSource.getRepository(TypeSellOrder);
		const oldSellOrder = await this.findRepository.findById(user.id);
		Object.assign(oldSellOrder, user);
		sellOrderRepository.save(oldSellOrder);
	}
}
