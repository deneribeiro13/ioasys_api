import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { SellOrder } from '../../entity/SellOrder';
import { TypeSellOrder } from '../entity/TypeSellOrder';

export class TypeFindRepository implements FindRepository {
	constructor() {}

	async findByEmail(email: string): Promise<SellOrder> {
		const sellOrderRepository = AppDataSource.getRepository(TypeSellOrder);
		const sellOrder = await sellOrderRepository.findOneBy({
			seller: { email },
		});
		return sellOrder;
	}

	async findById(id: string): Promise<SellOrder> {
		const sellOrderRepository = AppDataSource.getRepository(TypeSellOrder);
		const sellOrder = await sellOrderRepository.findOneBy({ id });
		return sellOrder;
	}

	async getAllSellOrders(): Promise<SellOrder[]> {
		const sellOrderRepository = AppDataSource.getRepository(TypeSellOrder);
		const sellOrder = await sellOrderRepository.find();
		return sellOrder;
	}
}
