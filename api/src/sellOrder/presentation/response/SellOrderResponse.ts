import { Product } from '../../../product/entity/Product';
import { ProductResponse } from '../../../product/presentation/response/ProductResponse';
import { Seller } from '../../../seller/entity/Seller';
import { SellerResponse } from '../../../seller/presentation/response/SellerResponse';
import { SellOrderPresentation } from '../../application/presentation/SellOrderPresentation';
import { SellOrder } from '../../entity/SellOrder';

export class SellOrderResponse implements SellOrderPresentation {
	id: string;

	number: number;
	product: ProductResponse;
	seller: SellerResponse;

	constructor(sellOrder?: SellOrder) {
		if (sellOrder) {
			this.id = sellOrder.id;
			this.product = new ProductResponse(sellOrder.product);
			this.seller = new SellerResponse(sellOrder.seller);
		}
	}

	of(sellORder: SellOrder): SellOrderResponse {
		return new SellOrderResponse(sellORder);
	}
}
