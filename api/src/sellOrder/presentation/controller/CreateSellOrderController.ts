import { Request, Response } from 'express';
import { InvalidParamsError } from '../../../common/presentation/errors/InvalidParamsError';
import { ProductNotFoundError } from '../../../common/presentation/errors/ProductNotFoundError';
import { ServerError } from '../../../common/presentation/errors/ServerError';
import { ProductRequest } from '../../../product/presentation/request/ProductResquest';
import { AddSellOrderUsecase } from '../../application/contract/AddSellOrderUsecase';
import { CreateSellOrderDTO } from '../../application/dto/CreateSellOrderDTO';
import { AuthMiddleware } from '../middleware/AuthMiddleware';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';

export class CreateSellOrderController {
	constructor(
		private createSellOrderUsecase: AddSellOrderUsecase,
		private authMiddlerware: AuthMiddleware,
		private verifyProduct: ProductRequest
	) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			if (!this.verifyProduct.verify(req.body.product))
				return res.status(409).json(http.Conflict(new InvalidParamsError()));
			if (!req.headers.authorization)
				return res.status(401).json(http.Unauthorized());

			const id = await this.authMiddlerware.handle(req, res);

			if (!id) return res.status(401).json(http.Unauthorized());

			req.body.seller = { id };
			const sellOrderResponse = await this.createSellOrderUsecase.execute(
				new CreateSellOrderDTO(req.body)
			);

			if (!sellOrderResponse)
				return res
					.status(400)
					.json(http.BadRequest(new ProductNotFoundError()));

			return res.status(200).json(http.Ok(sellOrderResponse));
		} catch (err) {
			console.log(err);
			return res
				.status(400)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
