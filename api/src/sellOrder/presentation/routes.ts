import { Router } from 'express';
import { createSellOrderController } from '../main';

const sellOrderRoute = Router();

sellOrderRoute.post('/', (req, res) => {
	return createSellOrderController.handle(req, res);
});

export { sellOrderRoute };
