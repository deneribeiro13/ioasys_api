import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { Product } from '../../entity/Product';
import { TypeProduct } from '../entity/TypeProduct';

export class TypeFindRepository implements FindRepository {
	constructor() {}

	async findByBarcode(barcode: string): Promise<Product> {
		const productRepository = AppDataSource.getRepository(TypeProduct);
		const product = await productRepository.findOneBy({ barcode });
		return product;
	}

	async findById(id: string): Promise<Product> {
		const productRepository = AppDataSource.getRepository(TypeProduct);
		const product = await productRepository.findOneBy({ id });
		return product;
	}

	async getAllProducts(): Promise<Product[]> {
		const productRepository = AppDataSource.getRepository(TypeProduct);
		const product = await productRepository.find();
		return product;
	}
}
