import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { UpdateRepository } from '../../application/db/contract/UpdateRepository';
import { Product } from '../../entity/Product';
import { TypeProduct } from '../entity/TypeProduct';

export class TypeUpdateRepository implements UpdateRepository {
	constructor(private findRepository: FindRepository) {}

	async update(product: Product): Promise<void> {
		const productRepository = AppDataSource.getRepository(TypeProduct);
		const oldProduct = await this.findRepository.findById(product.id);
		Object.assign(oldProduct, product);
		productRepository.save(oldProduct);
	}
}
