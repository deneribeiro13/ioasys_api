import { CreateRepository } from '../../application/db/contract/CreateRepository';
import { AppDataSource } from '../../../../config';
import { TypeProduct } from '../entity/TypeProduct';
import { Product } from '../../entity/Product';

export class TypeCreateRepository implements CreateRepository {
	constructor(private typeProduct: TypeProduct) {}

	async create(entity: Product): Promise<void> {
		const manager = this.typeProduct.of(entity);
		const managerRepository = AppDataSource.getRepository(TypeProduct);
		await managerRepository.save(manager);
	}
}
