import {
	Column,
	CreateDateColumn,
	DeleteDateColumn,
	Entity,
	PrimaryColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Product } from '../../entity/Product';

@Entity()
export class TypeProduct {
	@PrimaryColumn('uuid')
	id: string;

	@Column('double', { nullable: false })
	price: number;

	@Column('varchar', { length: 250, nullable: true })
	description: string;

	@Column('char', { length: 50, nullable: true })
	brand: string;

	@Column('double', { nullable: true })
	itens_in_storage: number;

	@Column('char', { length: 20, nullable: false })
	barcode: string;

	@CreateDateColumn()
	createdAt: string;

	@UpdateDateColumn()
	updatedAt: string;

	@DeleteDateColumn()
	deletedAt: string;

	constructor(product?: Product) {
		if (product) {
			this.id = product.id;
			this.price = product.price;
			this.description = product.description;
			this.brand = product.brand;
			this.itens_in_storage = product.itens_in_storage;
			this.barcode = product.barcode;
		}
	}

	of(product: Product): TypeProduct {
		return new TypeProduct(product);
	}
}
