import { ProductPresentation } from '../../application/presentation/ProductPresentation';
import { Product } from '../../entity/Product';

export class ProductResponse implements ProductPresentation {
	id: string;
	price: number;
	description: string;
	brand: string;
	buy_date: string;
	due_date: string;
	itens_in_storage: number;
	barcode: string;

	constructor(product?: Product) {
		Object.assign(this, product);
	}

	of(product: Product): ProductResponse {
		return new ProductResponse(product);
	}
}
