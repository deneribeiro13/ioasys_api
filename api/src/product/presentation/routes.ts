import { Router } from 'express';
import { createProductController, findAllProductsController } from '../main';

const productRoute = Router();

productRoute.post('/', (req, res) => {
	return createProductController.handle(req, res);
});

productRoute.get('/', (req, res) => {
	return findAllProductsController.handle(req, res);
});

export { productRoute };
