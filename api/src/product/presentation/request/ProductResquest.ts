export class ProductRequest {
	private verifyPrice(price: number): boolean {
		return price ? price > 0 && price < 50000 : false;
	}
	private verifyDescription(description: string): boolean {
		return description
			? description.length > 0 && description.length < 250
			: true;
	}
	private verifyBrand(brand: string): boolean {
		return brand ? brand.length > 0 && brand.length < 50 : false;
	}

	private verifyItensInStorage(itens: number): boolean {
		return itens ? itens > 0 && itens < 10000000 : true;
	}

	private verifyBarcode(barcode: string): boolean {
		return barcode ? barcode.length > 10 && barcode.length < 20 : false;
	}

	verify(body): boolean {
		return (
			body &&
			this.verifyPrice(body.price) &&
			this.verifyDescription(body.description) &&
			this.verifyBrand(body.brand) &&
			this.verifyItensInStorage(body.itens_in_storage) &&
			this.verifyBarcode(body.barcode)
		);
	}
}
