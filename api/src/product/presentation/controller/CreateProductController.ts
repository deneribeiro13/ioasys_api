import { Request, Response } from 'express';
import { InvalidParamsError } from '../../../common/presentation/errors/InvalidParamsError';
import { UnauthorizedError } from '../../../common/presentation/errors/UnauthorizedError';
import { AddOrUpdateProductUsecase } from '../../application/contract/AddProductUsecase';
import { CreateProductDTO } from '../../application/dto/CreateProductDTO';
import { AuthMiddleware } from '../middleware/AuthMiddleware';
import { ProductRequest } from '../request/ProductResquest';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';
import { ServerError } from '../../../common/presentation/errors/ServerError';

export class CreateProductController {
	constructor(
		private createProductUsecase: AddOrUpdateProductUsecase,
		private authMiddleware: AuthMiddleware,
		private verifyProduct: ProductRequest
	) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			if (!this.verifyProduct.verify(req.body))
				return res.status(409).json(http.Conflict(new InvalidParamsError()));
			if (!req.headers.authorization)
				return res.status(401).json(http.Unauthorized());

			const email = await this.authMiddleware.handle(req, res);
			if (!email) return res.status(401).json(http.Unauthorized());

			req.body.buyer = { email };

			const productResponse = await this.createProductUsecase.execute(
				new CreateProductDTO(req.body)
			);

			return res.status(200).json(http.Ok(productResponse));
		} catch (err) {
			console.log(err);
			return res
				.status(500)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
