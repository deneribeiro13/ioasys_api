import { Request, Response } from 'express';
import { FindAllProductsUsecase } from '../../application/contract/FindAllProductsUsecase';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';
import { ServerError } from '../../../common/presentation/errors/ServerError';

export class FindAllProductsController {
	constructor(private findAllProducts: FindAllProductsUsecase) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			const productResponse = await this.findAllProducts.execute();
			return res.status(200).json(http.Ok(productResponse));
		} catch (err) {
			return res
				.status(500)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
