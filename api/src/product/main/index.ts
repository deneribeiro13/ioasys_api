import { findBuyerRepository } from '../../buyer/main';
import { JwtAdapter } from '../../common/infrastructure/JwtAdapter';
import { uuidV4Adapter } from '../../common/infrastructure/uuidV4Adapter';
import { DbAddOrUpdateProductUsecase } from '../application/db/usecase/DbAddOrUpdateProductUsecase';
import { DbAuthenticationUsecase } from '../application/db/usecase/DbAuthenticationUsecase';
import { DbFindAllProductsUsecase } from '../application/db/usecase/DbFindAllProductsUsecase';
import { TypeCreateRepository } from '../persistence.typeorm/adapter/TypeCreateResotory';
import { TypeFindRepository } from '../persistence.typeorm/adapter/TypeFindRepository';
import { TypeUpdateRepository } from '../persistence.typeorm/adapter/TypeUpdateRepository';
import { TypeProduct } from '../persistence.typeorm/entity/TypeProduct';
import { CreateProductController } from '../presentation/controller/CreateProductController';
import { FindAllProductsController } from '../presentation/controller/FindAllProductsController';
import { AuthMiddleware } from '../presentation/middleware/AuthMiddleware';
import { ProductRequest } from '../presentation/request/ProductResquest';
import { ProductResponse } from '../presentation/response/ProductResponse';

const uuid = new uuidV4Adapter();
const jwt = new JwtAdapter();

// const mockProduct = new MockProductRepository();

export const typeProduct = new TypeProduct();
export const findProductRepository = new TypeFindRepository();
export const creatProductRepository = new TypeCreateRepository(typeProduct);
export const updateProductRepository = new TypeUpdateRepository(
	findProductRepository
);

const productResponse = new ProductResponse();
const addProductUsecase = new DbAddOrUpdateProductUsecase(
	findProductRepository,
	creatProductRepository,
	uuid,
	updateProductRepository
);
const verifyProduct = new ProductRequest();
const authCheck = new DbAuthenticationUsecase(findBuyerRepository, jwt);
const authMiddlerware = new AuthMiddleware(authCheck);
export const createProductController = new CreateProductController(
	addProductUsecase,
	authMiddlerware,
	verifyProduct
);

const findAllProductsUsecase = new DbFindAllProductsUsecase(
	findProductRepository,
	productResponse
);
export const findAllProductsController = new FindAllProductsController(
	findAllProductsUsecase
);
