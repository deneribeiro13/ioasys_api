export class Product {
	id: string;
	price: number;
	description: string;
	brand: string;
	itens_in_storage: number;
	barcode: string;

	constructor(props, id) {
		Object.assign(this, { ...props, id });
	}
}
