import { CreateRepository } from '../application/db/contract/CreateRepository';
import { DeleteRepository } from '../application/db/contract/DeleteRepository';
import { FindRepository } from '../application/db/contract/FindRepository';
import { UpdateRepository } from '../application/db/contract/UpdateRepository';
import { Product } from '../entity/Product';

export class MockProductRepository
	implements
		CreateRepository,
		FindRepository,
		DeleteRepository,
		UpdateRepository
{
	private products: Product[] = [];

	//Create
	async create(product: Product): Promise<void> {
		this.products.push(product);
	}

	//Find
	async findById(id: string): Promise<Product> {
		const product = await this.products.find((s) => s.id === id);

		return product;
	}
	async getAllProducts(): Promise<Product[]> {
		return await this.products;
	}
	async findByBarcode(barcode: string): Promise<Product> {
		const product = await this.products.find((s) => s.barcode === barcode);

		return product;
	}

	//Delete
	async delete(product: Product): Promise<void> {
		const i = this.products.findIndex((sel) => sel.barcode === product.barcode);
		if (i !== -1) {
			this.products[i] = null;
		}
	}

	//Update
	async update(product: Product): Promise<void> {
		const i = this.products.findIndex((sel) => sel.barcode === product.barcode);
		if (i !== -1) {
			this.products[i] = product;
		}
	}
}
