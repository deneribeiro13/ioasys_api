import { uuidAdapter } from '../../../../common/adapter/uuidAdapter';
import { Product } from '../../../entity/Product';
import { CreateProductDTO } from '../../dto/CreateProductDTO';
import { CreateRepository } from '../contract/CreateRepository';
import { FindRepository } from '../contract/FindRepository';
import { ProductPresentation } from '../../presentation/ProductPresentation';
import { UpdateRepository } from '../contract/UpdateRepository';
import { AddOrUpdateProductUsecase } from '../../contract/AddOrUpdateProductUsecase';

export class DbAddOrUpdateProductUsecase implements AddOrUpdateProductUsecase {
	constructor(
		private findRepository: FindRepository,
		private insertRepository: CreateRepository,
		private uuidAdapter: uuidAdapter,
		private updateRepository: UpdateRepository
	) {}

	async execute(dto: CreateProductDTO): Promise<Product> {
		const productExists = await this.findRepository.findByBarcode(dto.barcode);

		if (productExists) {
			productExists.itens_in_storage += dto.itens_in_storage;
			await this.updateRepository.update(productExists);
			return productExists;
		}

		const product = new Product(dto, this.uuidAdapter.uuid());
		await this.insertRepository.create(product);
		return product;
	}
}
