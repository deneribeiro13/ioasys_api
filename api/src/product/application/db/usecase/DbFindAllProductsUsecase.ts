import { Product } from '../../../entity/Product';
import { FindAllProductsUsecase } from '../../contract/FindAllProductsUsecase';
import { ProductPresentation } from '../../presentation/ProductPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindAllProductsUsecase implements FindAllProductsUsecase {
	constructor(
		private findRepository: FindRepository,
		private productPresentaion: ProductPresentation
	) {}

	async execute(): Promise<ProductPresentation[]> {
		const products = await this.findRepository.getAllProducts();
		return products.map((product) => {
			return this.productPresentaion.of(product);
		});
	}
}
