import { FindRepository } from '../../../../buyer/application/db/contract/FindRepository';
import { TokenAdapter } from '../../../../common/adapter/TokenAdapter';
import { AuthenticationUsecase } from '../../contract/AuthenticationUsecase';

export class DbAuthenticationUsecase implements AuthenticationUsecase {
	constructor(
		private findBuyerByEmail: FindRepository,
		private tokenAdapter: TokenAdapter
	) {}

	async execute(token: string, key: string): Promise<string> {
		const decoded = this.tokenAdapter.verify(token, key);
		if (!decoded) return null;
		const buyer = await this.findBuyerByEmail.findByEmail(decoded.email);
		if (!buyer) return null;
		return buyer.email;
	}
}
