import { UpdateProductUsecase } from '../../contract/UpdateProductUsecase';
import { CreateProductDTO } from '../../dto/CreateProductDTO';
import { ProductPresentation } from '../../presentation/ProductPresentation';
import { FindRepository } from '../contract/FindRepository';
import { UpdateRepository } from '../contract/UpdateRepository';

export class DbUpdateProductUsecase implements UpdateProductUsecase {
	constructor(
		private findRepository: FindRepository,
		private updateRepository: UpdateRepository,
		private productPresentation: ProductPresentation
	) {}

	async execute(dto: CreateProductDTO): Promise<ProductPresentation> {
		const product = await this.findRepository.findByBarcode(dto.barcode);
		if (!product) {
			return null;
		}
		Object.assign(product, dto);
		await this.updateRepository.update(product);
		return this.productPresentation.of(product);
	}
}
