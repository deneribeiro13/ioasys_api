import { FindProductByIdUsecase } from '../../contract/FindProductByIdUsecase';
import { FindRepository } from '../contract/FindRepository';
import { ProductPresentation } from '../../presentation/ProductPresentation';

export class DbFindProductByIdUsecase implements FindProductByIdUsecase {
	constructor(
		private findRepository: FindRepository,
		private productPresentation: ProductPresentation
	) {}

	async execute(id: string): Promise<ProductPresentation> {
		const product = await this.findRepository.findById(id);
		return this.productPresentation.of(product);
	}
}
