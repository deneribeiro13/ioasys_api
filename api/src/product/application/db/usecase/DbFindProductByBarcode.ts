import { Product } from '../../../entity/Product';
import { ProductResponse } from '../../../presentation/response/ProductResponse';
import { FindProductByBarcodeUsecase } from '../../contract/FindProductByBarcodeUsecase';
import { FindRepository } from '../contract/FindRepository';

export class DbFindProductByBarcodeUsecase
	implements FindProductByBarcodeUsecase
{
	constructor(
		private findRepository: FindRepository,
		private productResponse: ProductResponse
	) {}

	async execute(barcode: string): Promise<Product> {
		const product = await this.findRepository.findByBarcode(barcode);
		return this.productResponse.of(product);
	}
}
