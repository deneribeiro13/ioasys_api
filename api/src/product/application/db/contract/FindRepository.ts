import { Product } from '../../../entity/Product';

export interface FindRepository {
	findByBarcode(barcode: string): Promise<Product>;
	findById(id: string): Promise<Product>;
	getAllProducts(): Promise<Product[]>;
}
