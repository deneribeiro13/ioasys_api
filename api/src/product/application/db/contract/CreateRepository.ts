import { Product } from '../../../entity/Product';

export interface CreateRepository {
	create(entity: Product): Promise<void>;
}
