import { Product } from '../../../entity/Product';

export interface DeleteRepository {
	delete(product: Product): Promise<void>;
}
