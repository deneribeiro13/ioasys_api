import { Product } from '../../../entity/Product';

export interface UpdateRepository {
	update(dto: Product): Promise<void>;
}
