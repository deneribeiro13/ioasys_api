import { Product } from '../../entity/Product';

export interface ProductPresentation {
	of(product: Product): ProductPresentation;
}
