import { CreateProductDTO } from './CreateProductDTO';

export class UpdateProductDTO {
	product: CreateProductDTO;
	productId: string;

	constructor(props: UpdateProductDTO) {
		Object.assign(this, props);
	}
}
