export class CreateProductDTO {
	price: number;
	description: string;
	brand: string;
	itens_in_storage: number;
	barcode: string;

	constructor(props: CreateProductDTO) {
		Object.assign(this, props);
	}
}
