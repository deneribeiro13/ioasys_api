export interface DeleteProductUsecase {
	execute(id: string): Promise<void>;
}
