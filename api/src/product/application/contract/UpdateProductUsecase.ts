import { CreateProductDTO } from '../dto/CreateProductDTO';
import { ProductPresentation } from '../presentation/ProductPresentation';

export interface UpdateProductUsecase {
	execute(dto: CreateProductDTO): Promise<ProductPresentation>;
}
