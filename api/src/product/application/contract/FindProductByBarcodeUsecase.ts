import { Product } from '../../entity/Product';

export interface FindProductByBarcodeUsecase {
	execute(barcode: string): Promise<Product>;
}
