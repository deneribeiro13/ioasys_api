import { ProductPresentation } from '../presentation/ProductPresentation';

export interface FindProductByIdUsecase {
	execute(id: string): Promise<ProductPresentation>;
}
