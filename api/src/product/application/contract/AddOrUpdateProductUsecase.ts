import { CreateProductDTO } from '../dto/CreateProductDTO';
import { Product } from '../../entity/Product';

export interface AddOrUpdateProductUsecase {
	execute(dto: CreateProductDTO): Promise<Product>;
}
