import { ProductPresentation } from '../presentation/ProductPresentation';

export interface FindAllProductsUsecase {
	execute(): Promise<ProductPresentation[]>;
}
