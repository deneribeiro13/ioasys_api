import { CreateManagerDTO } from '../../../manager/application/dto/CreateManagerDTO';

export class CreateSellerDTO {
	email: string;
	password: string;
	name: string;
	age: number;
	phone: number;
	salary: number;
	comision_porcentage: number;
	manager: CreateManagerDTO;

	constructor(props: CreateSellerDTO) {
		Object.assign(this, props);
	}
}
