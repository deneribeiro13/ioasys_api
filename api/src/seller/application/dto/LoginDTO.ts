export class LoginDTO {
	token: string;

	email: string;
	password: string;

	constructor(props: LoginDTO) {
		Object.assign(this, props);
	}
}
