import { CreateSellerDTO } from './CreateSellerDTO';

export class UpdateSellerDTO {
	seller: CreateSellerDTO;
	sellserId: string;

	constructor(props: UpdateSellerDTO) {
		Object.assign(this, props);
	}
}
