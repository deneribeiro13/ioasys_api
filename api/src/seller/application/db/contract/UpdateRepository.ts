import { Seller } from '../../../entity/Seller';

export interface UpdateRepository {
	update(seller: Seller): Promise<void>;
}
