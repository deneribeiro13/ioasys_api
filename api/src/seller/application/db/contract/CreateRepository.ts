import { Seller } from '../../../entity/Seller';

export interface CreateRepository {
	create(seller: Seller): Promise<void>;
}
