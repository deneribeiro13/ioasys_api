import { Seller } from '../../../entity/Seller';

export interface DeleteRepository {
	delete(seller: Seller): Promise<void>;
}
