import { Seller } from '../../../entity/Seller';

export interface FindRepository {
	findByEmail(email: string): Promise<Seller>;
	findById(id: string): Promise<Seller>;
	getAllSellers(): Promise<Seller[]>;
}
