import { HashAdapter } from '../../../../common/adapter/HashAdapter';
import { uuidAdapter } from '../../../../common/adapter/uuidAdapter';
import { FindManagerByEmailUsecase } from '../../../../manager/application/contract/FindManagerByEmailUsecase';
import { Seller } from '../../../entity/Seller';
import { AddSellerUsecase } from '../../contract/AddSellerUsecase';
import { CreateSellerDTO } from '../../dto/CreateSellerDTO';
import { SellerPresentation } from '../../presentation/SellerPresentation';
import { CreateRepository } from '../contract/CreateRepository';
import { FindRepository } from '../contract/FindRepository';

export class DbAddSellerUsecase implements AddSellerUsecase {
	constructor(
		private findRepository: FindRepository,
		private insertRepository: CreateRepository,
		private uuidAdapter: uuidAdapter,
		private hashAdapter: HashAdapter,
		private findManagerByEmail: FindManagerByEmailUsecase,
		private sellerPresentation: SellerPresentation
	) {}

	async execute(dto: CreateSellerDTO): Promise<SellerPresentation> {
		const sellerExists = await this.findRepository.findByEmail(dto.email);

		if (sellerExists) return null;

		dto.password = await this.hashAdapter.generate(dto.password);
		const id = await this.uuidAdapter.uuid();
		const seller = new Seller(dto, id);
		await this.insertRepository.create(seller);

		return this.sellerPresentation.of(seller);
	}
}
