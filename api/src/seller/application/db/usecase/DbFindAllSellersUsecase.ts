import { Seller } from '../../../entity/Seller';
import { FindAllSellersUsecase } from '../../contract/FindAllSellersUsecase';
import { SellerPresentation } from '../../presentation/SellerPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindAllSellersUsecase implements FindAllSellersUsecase {
	constructor(
		private findRepository: FindRepository,
		private sellerPresentaion: SellerPresentation
	) {}

	async execute(): Promise<SellerPresentation[]> {
		const sellers = await this.findRepository.getAllSellers();
		return sellers.map((seller) => {
			return this.sellerPresentaion.of(seller);
		});
	}
}
