import { FindSellerByEmailUsecase } from '../../contract/FindSellerByEmailUsecase';
import { SellerPresentation } from '../../presentation/SellerPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindSellerByEmailUsecase implements FindSellerByEmailUsecase {
	constructor(
		private findRepository: FindRepository,
		private sellerPresentaion: SellerPresentation
	) {}

	async execute(email: string): Promise<SellerPresentation> {
		const seller = await this.findRepository.findByEmail(email);
		return this.sellerPresentaion.of(seller);
	}
}
