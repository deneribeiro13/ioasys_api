import { FindSellerByIdUsecase } from '../../contract/FindSellerByIdUsecase';
import { SellerPresentation } from '../../presentation/SellerPresentation';
import { FindRepository } from '../contract/FindRepository';

export class DbFindSellerByIdUsecase implements FindSellerByIdUsecase {
	constructor(
		private findRepository: FindRepository,
		private sellerPresentaion: SellerPresentation
	) {}

	async execute(email: string): Promise<SellerPresentation> {
		const seller = await this.findRepository.findById(email);
		return this.sellerPresentaion.of(seller);
	}
}
