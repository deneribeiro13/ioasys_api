import { Seller } from '../../entity/Seller';

export interface SellerPresentation {
	of(seller: Seller): SellerPresentation;
}
