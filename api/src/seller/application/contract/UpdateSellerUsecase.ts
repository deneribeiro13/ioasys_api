import { UpdateSellerDTO } from '../dto/UpdateSellerDTO';

export interface UpdateSellerUsecase {
	execute(dto: UpdateSellerDTO): Promise<void>;
}
