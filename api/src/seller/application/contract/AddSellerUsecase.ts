import { CreateSellerDTO } from '../dto/CreateSellerDTO';
import { SellerPresentation } from '../presentation/SellerPresentation';

export interface AddSellerUsecase {
	execute(dto: CreateSellerDTO): Promise<SellerPresentation>;
}
