import { LoginPresentation } from '../../../common/presentation/response/LoginPresentation';
import { LoginDTO } from '../dto/LoginDTO';

export interface Login {
	execute(dto: LoginDTO): Promise<LoginPresentation>;
}
