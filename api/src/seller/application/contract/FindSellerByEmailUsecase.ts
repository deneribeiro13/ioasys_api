import { SellerPresentation } from '../presentation/SellerPresentation';

export interface FindSellerByEmailUsecase {
	execute(email: string): Promise<SellerPresentation>;
}
