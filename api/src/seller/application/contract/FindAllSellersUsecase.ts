import { SellerPresentation } from '../presentation/SellerPresentation';

export interface FindAllSellersUsecase {
	execute(): Promise<SellerPresentation[]>;
}
