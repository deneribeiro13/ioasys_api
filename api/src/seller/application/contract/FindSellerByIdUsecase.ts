import { SellerPresentation } from '../presentation/SellerPresentation';

export interface FindSellerByIdUsecase {
	execute(id: string): Promise<SellerPresentation>;
}
