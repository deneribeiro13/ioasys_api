export interface DeleteSellerUsecase {
	execute(id: string): Promise<void>;
}
