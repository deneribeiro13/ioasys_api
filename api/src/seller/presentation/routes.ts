import { Router } from 'express';
import {
	authController,
	createSellerController,
	findAllSellersController,
} from '../main';

const sellerRoute = Router();

sellerRoute.post('/', (req, res) => {
	return createSellerController.handle(req, res);
});

sellerRoute.post('/login', (req, res) => {
	return authController.handle(req, res);
});

sellerRoute.get('/', (req, res) => {
	return findAllSellersController.handle(req, res);
});

export { sellerRoute };
