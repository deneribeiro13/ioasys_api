import { Request, Response } from 'express';
import { EmailInUseError } from '../../../common/presentation/errors/EmailInUseError';
import { InvalidParamsError } from '../../../common/presentation/errors/InvalidParamsError';
import { UnauthorizedError } from '../../../common/presentation/errors/UnauthorizedError';
import { UserRequest } from '../../../common/presentation/request/UserResquest';
import { AddSellerUsecase } from '../../application/contract/AddSellerUsecase';
import { CreateSellerDTO } from '../../application/dto/CreateSellerDTO';
import { AuthMiddleware } from '../middleware/AuthMiddleware';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';
import { ServerError } from '../../../common/presentation/errors/ServerError';

export class CreateSellerController {
	constructor(
		private addSeller: AddSellerUsecase,
		private authMiddleware: AuthMiddleware,
		private verifyRequest: UserRequest
	) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			if (!this.verifyRequest.verify(req.body))
				return res.status(409).json(http.Conflict(new InvalidParamsError()));

			if (!req.headers.authorization)
				return res.status(401).json(http.Unauthorized());

			const email = await this.authMiddleware.handle(req, res);
			if (!email) return res.status(401).json(http.Unauthorized());

			req.body.manager = { email };

			const sellerResponse = await this.addSeller.execute(
				new CreateSellerDTO(req.body)
			);
			if (!sellerResponse)
				return res.status(400).json(http.BadRequest(new EmailInUseError()));

			return res.status(200).json(http.Ok(sellerResponse));
		} catch (err) {
			return res
				.status(500)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
