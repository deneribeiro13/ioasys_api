import { Request, Response } from 'express';
import { FindAllSellersUsecase } from '../../application/contract/FindAllSellersUsecase';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';
import { ServerError } from '../../../common/presentation/errors/ServerError';

export class FindAllSellersController {
	constructor(private findAllSellers: FindAllSellersUsecase) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			const sellerResponse = await this.findAllSellers.execute();
			return res.status(200).json(http.Ok(sellerResponse));
		} catch (err) {
			return res
				.status(400)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
