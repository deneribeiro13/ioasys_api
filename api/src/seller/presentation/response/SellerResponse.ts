import { Manager } from '../../../manager/entity/Manager';
import { SellerPresentation } from '../../application/presentation/SellerPresentation';
import { Seller } from '../../entity/Seller';

export class SellerResponse implements SellerPresentation {
	id: string;
	email: string;
	name: string;
	age: number;
	phone: number;
	salary: number;
	comision_porcentage: number;
	manager: Manager;

	constructor(seller?: Seller) {
		if (seller) {
			this.id = seller.id;
			this.email = seller.email;
			this.name = seller.name;
			this.age = seller.age;
			this.phone = seller.phone;
			this.salary = seller.salary;
			this.comision_porcentage = seller.comision_porcentage;
			this.manager = seller.manager;
		}
	}

	of(seller: Seller): SellerResponse {
		return new SellerResponse(seller);
	}
}
