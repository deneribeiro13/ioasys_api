import { User } from '../../common/entity/User';
import { Manager } from '../../manager/entity/Manager';

export class Seller extends User {
	comision_porcentage: number;
	manager: Manager;
	constructor(props, id: string) {
		super({ ...props, id });
		this.comision_porcentage = props.comision_porcentage;
		this.manager = props.manager;
	}
}
