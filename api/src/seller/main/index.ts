import { CryptoAdapter } from '../../common/infrastructure/CryptoAdapter';
import { JwtAdapter } from '../../common/infrastructure/JwtAdapter';
import { uuidV4Adapter } from '../../common/infrastructure/uuidV4Adapter';
import { UserRequest } from '../../common/presentation/request/UserResquest';
import { DbFindManagerByEmailUsecase } from '../../manager/application/db/usecase/DbFindManagerByEmailUsecase';
import { findManagerRepository } from '../../manager/main';
import { ManagerResponse } from '../../manager/presentation/response/ManagerResponse';
import { DbAddSellerUsecase } from '../application/db/usecase/DbAddSellerUsecase';
import { DbAuthenticationUsecase } from '../application/db/usecase/DbAuthenticationUsecase';
import { DbFindAllSellersUsecase } from '../application/db/usecase/DbFindAllSellersUsecase';
import { DbLogin } from '../application/db/usecase/DbLogin';
import { TypeCreateRepository } from '../persistence.typeorm/adapter/TypeCreateRepository';
import { TypeFindRepository } from '../persistence.typeorm/adapter/TypeFindRepository';
import { TypeUpdateRepository } from '../persistence.typeorm/adapter/TypeUpdateRepository';
import { TypeSeller } from '../persistence.typeorm/entity/TypeSeller';
import { AuthenticantionController } from '../presentation/controller/AuthenticationController';
import { CreateSellerController } from '../presentation/controller/CreateSellerController';
import { FindAllSellersController } from '../presentation/controller/FindAllSellersController';
import { AuthMiddleware } from '../presentation/middleware/AuthMiddleware';
import { LoginResponse } from '../presentation/response/LoginResponse';
import { SellerResponse } from '../presentation/response/SellerResponse';

const crypto = new CryptoAdapter(10);
const uuid = new uuidV4Adapter();
const jwt = new JwtAdapter();

// const mockSeller = new MockSellerRepository();

const typeSeller = new TypeSeller();
export const findSellerRepository = new TypeFindRepository();
export const createSellerRepository = new TypeCreateRepository(typeSeller);
export const updateSellerRepository = new TypeUpdateRepository(
	findSellerRepository
);

const managerResponse = new ManagerResponse();
const findManagerByEmail = new DbFindManagerByEmailUsecase(
	findManagerRepository,
	managerResponse
);

const sellerResponse = new SellerResponse();
const addSellerUsecase = new DbAddSellerUsecase(
	findSellerRepository,
	createSellerRepository,
	uuid,
	crypto,
	findManagerByEmail,
	sellerResponse
);
const authCkeck = new DbAuthenticationUsecase(findManagerRepository, jwt);
const authMiddleware = new AuthMiddleware(authCkeck);
const userRequest = new UserRequest();
export const createSellerController = new CreateSellerController(
	addSellerUsecase,
	authMiddleware,
	userRequest
);

const findAllSellersUsecase = new DbFindAllSellersUsecase(
	findSellerRepository,
	sellerResponse
);
export const findAllSellersController = new FindAllSellersController(
	findAllSellersUsecase
);

const loginResponse = new LoginResponse();
const login = new DbLogin(crypto, findSellerRepository, jwt, loginResponse);
export const authController = new AuthenticantionController(login, userRequest);
