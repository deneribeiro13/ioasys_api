import { CreateRepository } from '../application/db/contract/CreateRepository';
import { DeleteRepository } from '../application/db/contract/DeleteRepository';
import { FindRepository } from '../application/db/contract/FindRepository';
import { UpdateRepository } from '../application/db/contract/UpdateRepository';
import { Seller } from '../entity/Seller';

export class MockSellerRepository
	implements
		CreateRepository,
		FindRepository,
		DeleteRepository,
		UpdateRepository
{
	private sellers: Seller[] = [];

	//Create
	async create(seller: Seller): Promise<void> {
		this.sellers.push(seller);
	}

	//Find
	async findById(id: string): Promise<Seller> {
		const seller = await this.sellers.find((s) => s.id === id);

		return seller;
	}
	async getAllSellers(): Promise<Seller[]> {
		return await this.sellers;
	}
	async findByEmail(email: string): Promise<Seller> {
		const seller = await this.sellers.find((s) => s.email === email);

		return seller;
	}

	//Delete
	async delete(seller: Seller): Promise<void> {
		const i = this.sellers.findIndex((sel) => sel.email === seller.email);
		if (i !== -1) {
			this.sellers[i] = null;
		}
	}

	//Update
	async update(seller: Seller): Promise<void> {
		const i = this.sellers.findIndex((sel) => sel.email === seller.email);
		if (i !== -1) {
			this.sellers[i] = seller;
		}
	}
}
