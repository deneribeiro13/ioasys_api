import {
	Column,
	CreateDateColumn,
	DeleteDateColumn,
	Entity,
	JoinColumn,
	OneToOne,
	PrimaryColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Manager } from '../../../manager/entity/Manager';
import { TypeManager } from '../../../manager/persistence.typeorm/entity/TypeManager';
import { Seller } from '../../entity/Seller';

@Entity()
export class TypeSeller {
	@PrimaryColumn('uuid')
	id: string;

	@Column('char', { length: 50, nullable: false })
	email: string;

	@Column('char', { length: 60, nullable: false })
	password: string;

	@Column('char', { length: 50, nullable: true })
	name: string;

	@Column('integer', { nullable: true })
	age: number;

	@Column('char', { length: 15, nullable: true })
	phone: string;

	@Column('double', { nullable: true })
	salary: number;

	@Column('double', { nullable: true })
	comision_porcentage: number;

	@OneToOne(() => TypeManager)
	@JoinColumn()
	manager: Manager;

	@CreateDateColumn()
	createdAt: string;

	@UpdateDateColumn()
	updatedAt: string;

	@DeleteDateColumn()
	deletedAt: string;

	constructor(seller?: Seller) {
		if (seller) {
			this.id = seller.id;
			this.password = seller.password;
			this.email = seller.email;
			this.name = seller.name;
			this.age = seller.age;
			this.phone = seller.phone;
			this.salary = seller.salary;
			this.comision_porcentage = seller.comision_porcentage;
			this.manager = seller.manager;
		}
	}

	of(seller: Seller): TypeSeller {
		return new TypeSeller(seller);
	}
}
