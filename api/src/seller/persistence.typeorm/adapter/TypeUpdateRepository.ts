import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { UpdateRepository } from '../../application/db/contract/UpdateRepository';
import { Seller } from '../../entity/Seller';
import { TypeSeller } from '../entity/TypeSeller';

export class TypeUpdateRepository implements UpdateRepository {
	constructor(private findRepository: FindRepository) {}

	async update(user: Seller): Promise<void> {
		const sellerRepository = AppDataSource.getRepository(TypeSeller);
		const oldSeller = await this.findRepository.findById(user.id);
		Object.assign(oldSeller, user);
		sellerRepository.save(oldSeller);
	}
}
