import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { Seller } from '../../entity/Seller';
import { TypeSeller } from '../entity/TypeSeller';

export class TypeFindRepository implements FindRepository {
	constructor() {}

	async findByEmail(email: string): Promise<Seller> {
		const sellerRepository = AppDataSource.getRepository(TypeSeller);
		const seller = await sellerRepository.findOneBy({ email });
		return seller;
	}

	async findById(id: string): Promise<Seller> {
		const sellerRepository = AppDataSource.getRepository(TypeSeller);
		const seller = await sellerRepository.findOneBy({ id });
		return seller;
	}

	async getAllSellers(): Promise<Seller[]> {
		const sellerRepository = AppDataSource.getRepository(TypeSeller);
		const seller = await sellerRepository.find();
		return seller;
	}
}
