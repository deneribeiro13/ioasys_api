import { CreateRepository } from '../../application/db/contract/CreateRepository';
import { Seller } from '../../entity/Seller';
import { TypeSeller } from '../entity/TypeSeller';
import { AppDataSource } from '../../../../config';

export class TypeCreateRepository implements CreateRepository {
	constructor(private typeSeller: TypeSeller) {}

	async create(entity: Seller): Promise<void> {
		const seller = this.typeSeller.of(entity);
		const sellerRepository = AppDataSource.getRepository(TypeSeller);
		await sellerRepository.save(seller);
	}
}
