import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { UpdateRepository } from '../../application/db/contract/UpdateRepository';
import { BuyOrder } from '../../entity/BuyOrder';
import { TypeBuyOrder } from '../entity/TypeBuyOrder';

export class TypeUpdateRepository implements UpdateRepository {
	constructor(private findRepository: FindRepository) {}

	async update(user: BuyOrder): Promise<void> {
		const buyOrderRepository = AppDataSource.getRepository(TypeBuyOrder);
		const oldBuyOrder = await this.findRepository.findById(user.id);
		Object.assign(oldBuyOrder, user);
		buyOrderRepository.save(oldBuyOrder);
	}
}
