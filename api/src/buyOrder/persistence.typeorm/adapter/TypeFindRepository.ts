import { AppDataSource } from '../../../../config/';
import { FindRepository } from '../../application/db/contract/FindRepository';
import { BuyOrder } from '../../entity/BuyOrder';
import { TypeBuyOrder } from '../entity/TypeBuyOrder';

export class TypeFindRepository implements FindRepository {
	constructor() {}

	async findByEmail(email: string): Promise<BuyOrder> {
		const buyOrderRepository = AppDataSource.getRepository(TypeBuyOrder);
		const buyOrder = await buyOrderRepository.findOneBy({
			buyer: { email },
		});
		return buyOrder;
	}

	async findById(id: string): Promise<BuyOrder> {
		const buyOrderRepository = AppDataSource.getRepository(TypeBuyOrder);
		const buyOrder = await buyOrderRepository.findOneBy({ id });
		return buyOrder;
	}

	async getAllBuyOrders(): Promise<BuyOrder[]> {
		const buyOrderRepository = AppDataSource.getRepository(TypeBuyOrder);
		const buyOrder = await buyOrderRepository.find();
		return buyOrder;
	}
}
