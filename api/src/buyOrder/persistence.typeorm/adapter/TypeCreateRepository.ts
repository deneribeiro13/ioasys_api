import { CreateRepository } from '../../application/db/contract/CreateRepository';
import { BuyOrder } from '../../entity/BuyOrder';
import { TypeBuyOrder } from '../entity/TypeBuyOrder';
import { AppDataSource } from '../../../../config';

export class TypeCreateRepository implements CreateRepository {
	constructor(private typeBuyOrder: TypeBuyOrder) {}

	async create(entity: BuyOrder): Promise<void> {
		const buyOrder = this.typeBuyOrder.of(entity);
		const buyOrderRepository = AppDataSource.getRepository(TypeBuyOrder);
		await buyOrderRepository.save(buyOrder);
	}
}
