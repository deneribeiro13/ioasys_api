import {
	Column,
	CreateDateColumn,
	DeleteDateColumn,
	Entity,
	JoinColumn,
	OneToOne,
	PrimaryColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Product } from '../../../product/entity/Product';
import { TypeProduct } from '../../../product/persistence.typeorm/entity/TypeProduct';
import { Buyer } from '../../../buyer/entity/Buyer';
import { TypeBuyer } from '../../../buyer/persistence.typeorm/entity/TypeBuyer';
import { BuyOrder } from '../../entity/BuyOrder';

@Entity()
export class TypeBuyOrder {
	@PrimaryColumn('uuid')
	id: string;

	@Column('integer', { nullable: false })
	number: number;

	@OneToOne(() => TypeProduct, { nullable: false })
	@JoinColumn()
	product: Product;

	@OneToOne(() => TypeBuyer, { nullable: false })
	@JoinColumn()
	buyer: Buyer;

	@CreateDateColumn()
	createdAt: string;

	@UpdateDateColumn()
	updatedAt: string;

	@DeleteDateColumn()
	deletedAt: string;

	constructor(buyOrder?: BuyOrder) {
		if (buyOrder) {
			this.id = buyOrder.id;
			this.number = buyOrder.number;
			this.product = buyOrder.product;
			this.buyer = buyOrder.buyer;
		}
	}

	of(buyOrder: BuyOrder): TypeBuyOrder {
		return new TypeBuyOrder(buyOrder);
	}
}
