import { CreateRepository } from '../application/db/contract/CreateRepository';
import { DeleteRepository } from '../application/db/contract/DeleteRepository';
import { FindRepository } from '../application/db/contract/FindRepository';
import { UpdateRepository } from '../application/db/contract/UpdateRepository';
import { BuyOrder } from '../entity/BuyOrder';

export class MockBuyOrderRepository
	implements
		CreateRepository,
		FindRepository,
		DeleteRepository,
		UpdateRepository
{
	private buyOrders: BuyOrder[] = [];

	//Create
	async create(buyOrder: BuyOrder): Promise<void> {
		this.buyOrders.push(buyOrder);
	}

	//Find
	async findById(id: string): Promise<BuyOrder> {
		const buyOrder = await this.buyOrders.find((s) => s.id === id);

		return buyOrder;
	}
	async getAllBuyOrders(): Promise<BuyOrder[]> {
		return await this.buyOrders;
	}
	async findByEmail(email: string): Promise<BuyOrder> {
		const buyOrder = await this.buyOrders.find((s) => s.buyer.email === email);

		return buyOrder;
	}

	//Delete
	async delete(buyOrder: BuyOrder): Promise<void> {
		const i = this.buyOrders.findIndex((sel) => sel.id === buyOrder.id);
		if (i !== -1) {
			this.buyOrders[i] = null;
		}
	}

	//Update
	async update(buyOrder: BuyOrder): Promise<void> {
		const i = this.buyOrders.findIndex((sel) => sel.id === buyOrder.id);
		if (i !== -1) {
			this.buyOrders[i] = buyOrder;
		}
	}
}
