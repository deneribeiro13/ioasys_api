import { BuyerResponse } from '../../../buyer/presentation/response/BuyerResponse';
import { ProductResponse } from '../../../product/presentation/response/ProductResponse';
import { BuyOrderPresentation } from '../../application/presentation/BuyOrderPresentation';
import { BuyOrder } from '../../entity/BuyOrder';

export class BuyOrderResponse implements BuyOrderPresentation {
	id: string;
	number: number;
	product: ProductResponse;
	buyer: BuyerResponse;

	constructor(buyOrder?: BuyOrder) {
		if (buyOrder) {
			this.id = buyOrder.id;
			this.product = new ProductResponse(buyOrder.product);
			this.buyer = new BuyerResponse(buyOrder.buyer);
		}
	}

	of(buyOrder: BuyOrder): BuyOrderResponse {
		return new BuyOrderResponse(buyOrder);
	}
}
