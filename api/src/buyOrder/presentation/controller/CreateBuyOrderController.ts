import { Request, Response } from 'express';
import { InvalidParamsError } from '../../../common/presentation/errors/InvalidParamsError';
import { ServerError } from '../../../common/presentation/errors/ServerError';
import { ProductRequest } from '../../../product/presentation/request/ProductResquest';
import { AddBuyOrderUsecase } from '../../application/contract/AddBuyOrderUsecase';
import { CreateBuyOrderDTO } from '../../application/dto/CreateBuyOrderDTO';
import { AuthMiddleware } from '../middleware/AuthMiddleware';
import { HttpResponseFactory as http } from '../../../common/helper/HttpResponseFactory';

export class CreateBuyOrderController {
	constructor(
		private createBuyOrderUsecase: AddBuyOrderUsecase,
		private authMiddlerware: AuthMiddleware,
		private verifyProduct: ProductRequest
	) {}

	async handle(req: Request, res: Response): Promise<Response> {
		try {
			if (!this.verifyProduct.verify(req.body.product))
				return res.status(409).json(http.Conflict(new InvalidParamsError()));

			if (!req.headers.authorization)
				return res.status(401).json(http.Unauthorized());

			const id = await this.authMiddlerware.handle(req, res);

			if (!id) {
				return res.status(401).json(http.Unauthorized());
			}

			req.body.buyer = { id };

			const buyOrderResponse = await this.createBuyOrderUsecase.execute(
				new CreateBuyOrderDTO(req.body)
			);

			if (!buyOrderResponse) {
				return res.status(401).json(http.Unauthorized());
			}

			return res.status(200).json(http.Ok(buyOrderResponse));
		} catch (err) {
			console.log(err);
			return res
				.status(500)
				.json(http.InternalServerError(new ServerError(err)));
		}
	}
}
