import { Router } from 'express';
import { createBuyOrderController } from '../main';

const buyOrderRoute = Router();

buyOrderRoute.post('/', (req, res) => {
	return createBuyOrderController.handle(req, res);
});

export { buyOrderRoute };
