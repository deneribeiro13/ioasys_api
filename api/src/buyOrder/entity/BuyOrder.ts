import { Buyer } from '../../buyer/entity/Buyer';
import { Product } from '../../product/entity/Product';

export class BuyOrder {
	id: string;

	number: number;
	product: Product;
	buyer: Buyer;

	constructor(props, id: string) {
		Object.assign(this, { ...props, id });
	}
}
