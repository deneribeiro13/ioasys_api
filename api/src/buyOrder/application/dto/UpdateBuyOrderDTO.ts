import { Buyer } from '../../../buyer/entity/Buyer';
import { CreateBuyOrderDTO } from './CreateBuyOrderDTO';

export class UpdateBuyOrderDTO {
	buyOrder: CreateBuyOrderDTO;
	buyOrderId: string;

	constructor(props: UpdateBuyOrderDTO) {
		Object.assign(this, props);
	}
}
