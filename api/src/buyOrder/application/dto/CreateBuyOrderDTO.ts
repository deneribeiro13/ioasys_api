import { Buyer } from '../../../buyer/entity/Buyer';
import { Product } from '../../../product/entity/Product';

export class CreateBuyOrderDTO {
	buyer: Buyer;
	number: number;
	product: Product;

	constructor(props: CreateBuyOrderDTO) {
		Object.assign(this, props);
	}
}
