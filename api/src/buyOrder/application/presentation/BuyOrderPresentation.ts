import { BuyOrder } from '../../entity/BuyOrder';

export interface BuyOrderPresentation {
	of(buyOrder: BuyOrder): BuyOrderPresentation;
}
