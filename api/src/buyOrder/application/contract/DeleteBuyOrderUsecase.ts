export interface DeleteBuyOrderUsecase {
	execute(id: string): Promise<void>;
}
