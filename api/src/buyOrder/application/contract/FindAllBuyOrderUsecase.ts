import { BuyOrderPresentation } from '../presentation/BuyOrderPresentation';

export interface FindAllBuyOrderUsecase {
	execute(): Promise<BuyOrderPresentation[]>;
}
