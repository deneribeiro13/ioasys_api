import { BuyOrderPresentation } from '../presentation/BuyOrderPresentation';

export interface FindBuyOrderByIdUsecase {
	execute(id: string): Promise<BuyOrderPresentation>;
}
