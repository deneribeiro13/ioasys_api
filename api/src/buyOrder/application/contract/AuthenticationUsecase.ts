export interface AuthenticationUsecase {
	execute(token: string, key: string): Promise<string>;
}
