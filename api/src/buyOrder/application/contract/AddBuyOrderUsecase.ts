import { CreateBuyOrderDTO } from '../dto/CreateBuyOrderDTO';
import { BuyOrderPresentation } from '../presentation/BuyOrderPresentation';

export interface AddBuyOrderUsecase {
	execute(dto: CreateBuyOrderDTO): Promise<BuyOrderPresentation>;
}
