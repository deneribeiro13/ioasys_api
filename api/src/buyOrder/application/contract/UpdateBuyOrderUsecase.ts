import { UpdateBuyOrderDTO } from '../dto/UpdateBuyOrderDTO';

export interface UpdateBuyOrder {
	execute(dto: UpdateBuyOrderDTO): Promise<void>;
}
