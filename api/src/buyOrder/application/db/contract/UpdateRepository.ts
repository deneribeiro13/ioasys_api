import { BuyOrder } from '../../../entity/BuyOrder';

export interface UpdateRepository {
	update(buyOrder: BuyOrder): Promise<void>;
}
