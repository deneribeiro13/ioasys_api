import { BuyOrder } from '../../../entity/BuyOrder';

export interface CreateRepository {
	create(entity: BuyOrder): Promise<void>;
}
