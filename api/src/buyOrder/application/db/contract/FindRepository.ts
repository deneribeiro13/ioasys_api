import { BuyOrder } from '../../../entity/BuyOrder';

export interface FindRepository {
	findByEmail(email: string): Promise<BuyOrder>;
	findById(id: string): Promise<BuyOrder>;
	getAllBuyOrders(): Promise<BuyOrder[]>;
}
