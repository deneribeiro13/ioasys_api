import { BuyOrder } from '../../../entity/BuyOrder';

export interface DeleteRepository {
	delete(buyOrder: BuyOrder): Promise<void>;
}
