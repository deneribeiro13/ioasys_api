import { FindBuyerByEmailUsecase } from '../../../../buyer/application/contract/FindBuyerByEmailUsecase';
import { uuidAdapter } from '../../../../common/adapter/uuidAdapter';
import { AddOrUpdateProductUsecase } from '../../../../product/application/contract/AddOrUpdateProductUsecase';
import { UpdateProductUsecase } from '../../../../product/application/contract/UpdateProductUsecase';
import { BuyOrder } from '../../../entity/BuyOrder';
import { AddBuyOrderUsecase } from '../../contract/AddBuyOrderUsecase';
import { CreateBuyOrderDTO } from '../../dto/CreateBuyOrderDTO';
import { BuyOrderPresentation } from '../../presentation/BuyOrderPresentation';
import { CreateRepository } from '../contract/CreateRepository';

export class DbAddBuyOrderUsecase implements AddBuyOrderUsecase {
	constructor(
		private insertRepository: CreateRepository,
		private uuidAdapter: uuidAdapter,
		private addOrUpdateProduct: AddOrUpdateProductUsecase,
		private buyOrderPresentation: BuyOrderPresentation
	) {}

	async execute(dto: CreateBuyOrderDTO): Promise<BuyOrderPresentation> {
		dto.product.itens_in_storage = dto.number;
		const update = await this.addOrUpdateProduct.execute(dto.product);
		if (!update) return null;
		dto.product.id = update.id;
		const buyOrder = new BuyOrder(dto, this.uuidAdapter.uuid());

		await this.insertRepository.create(buyOrder);

		return this.buyOrderPresentation.of(buyOrder);
	}
}
