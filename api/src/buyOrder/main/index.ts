import { DbFindProductByIdUsecase } from '../../product/application/db/usecase/DbFindProductByIdUsecase';
import { ProductResponse } from '../../product/presentation/response/ProductResponse';
import { DbFindBuyerByEmailUsecase } from '../../buyer/application/db/usecase/DbFindBuyerByEmailUsecase';
import { BuyerResponse } from '../../buyer/presentation/response/BuyerResponse';
import { DbAddBuyOrderUsecase } from '../application/db/usecase/DbAddBuyOrderUsecase';
import { CreateBuyOrderController } from '../presentation/controller/CreateBuyOrderController';
import { BuyOrderResponse } from '../presentation/response/BuyOrderResponse';
import { uuidV4Adapter } from '../../common/infrastructure/uuidV4Adapter';
import { DbAuthenticationUsecase } from '../application/db/usecase/DbAuthenticationUsecase';
import { DbFindBuyerByIdUsecase } from '../../buyer/application/db/usecase/DbFindBuyerByIdUsecase';
import { JwtAdapter } from '../../common/infrastructure/JwtAdapter';
import { AuthMiddleware } from '../presentation/middleware/AuthMiddleware';
import { DbAddOrUpdateProductUsecase } from '../../product/application/db/usecase/DbAddOrUpdateProductUsecase';
import { ProductRequest } from '../../product/presentation/request/ProductResquest';
import { TypeBuyOrder } from '../persistence.typeorm/entity/TypeBuyOrder';
import { TypeFindRepository } from '../persistence.typeorm/adapter/TypeFindRepository';
import { TypeCreateRepository } from '../persistence.typeorm/adapter/TypeCreateRepository';
import { TypeUpdateRepository } from '../persistence.typeorm/adapter/TypeUpdateRepository';
import { findBuyerRepository } from '../../buyer/main';
import {
	findProductRepository,
	updateProductRepository,
} from '../../product/main';
import { creatProductRepository } from '../../product/main';

const uuid = new uuidV4Adapter();
const jwt = new JwtAdapter();
// const mockBuyOrder = new MockBuyOrderRepository();

const typeBuyOrder = new TypeBuyOrder();
export const findBuyOrderRepository = new TypeFindRepository();
export const createBuyOrderRepository = new TypeCreateRepository(typeBuyOrder);
export const updateBuyOrderRepository = new TypeUpdateRepository(
	findBuyOrderRepository
);

const buyerResponse = new BuyerResponse();
const findBuyerByEmail = new DbFindBuyerByEmailUsecase(
	findBuyerRepository,
	buyerResponse
);

const productResponse = new ProductResponse();
const findProductById = new DbFindProductByIdUsecase(
	findProductRepository,
	productResponse
);

const addOrUpdateProduct = new DbAddOrUpdateProductUsecase(
	findProductRepository,
	creatProductRepository,
	uuid,
	updateProductRepository
);
const sellOrderResponse = new BuyOrderResponse();
const addBuyOrderUsecase = new DbAddBuyOrderUsecase(
	createBuyOrderRepository,
	uuid,
	addOrUpdateProduct,
	sellOrderResponse
);

const findBuyerById = new DbFindBuyerByIdUsecase(
	findBuyerRepository,
	buyerResponse
);
const authCkeck = new DbAuthenticationUsecase(findBuyerRepository, jwt);
const authMiddlerware = new AuthMiddleware(authCkeck);
const verifyProduct = new ProductRequest();
export const createBuyOrderController = new CreateBuyOrderController(
	addBuyOrderUsecase,
	authMiddlerware,
	verifyProduct
);
