import { Router } from 'express';
import { buyerRoute } from './src/buyer/presentation/routes';
import { buyOrderRoute } from './src/buyOrder/presentation/routes';
import { managerRoute } from './src/manager/presentation/routes';
import { productRoute } from './src/product/presentation/routes';
import { sellerRoute } from './src/seller/presentation/routes';
import { sellOrderRoute } from './src/sellOrder/presentation/routes';

const router = Router();
router.use('/seller', sellerRoute);
router.use('/buyer', buyerRoute);
router.use('/manager', managerRoute);
router.use('/product', productRoute);
router.use('/sellOrder', sellOrderRoute);
router.use('/buyOrder', buyOrderRoute);

export { router };
