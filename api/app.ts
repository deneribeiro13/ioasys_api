import 'reflect-metadata';
import express from 'express';
import { AppDataSource } from './config/';
import { router } from './routes';

AppDataSource.initialize()
	.then(() => {
		console.log('Data Source has been initialized!');
	})
	.catch((err) => {
		console.error('Error during Data Source initialization:', err);
	});

const app = express();
app.use(express.json());

app.use('/api', router);
app.listen(3000, () => {
	console.log('Connected!');
});

// const manager = new TypeManager();
// manager.id = 'sdfasdfasd';
// manager.name = 'dedener';
// await AppDataSource.manager.save(manager);
